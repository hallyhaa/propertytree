/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.textext;

import java.awt.Dimension;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A subclass of java.text.Format for handling {@link Dimension} objects.
 *
 * @author Hallvard Ystad
 */
public class DimensionFormat extends Format {
  
  /** RegEx in use */
  private Pattern actuallyUsedFormat;
  /** RegEx used for parsing integer dimensions */
  private static final Pattern dimIntFormat =    Pattern.compile("(\\d+?)\\s*?(X|x)\\s*?(\\d+?)");
  /**
   * RegEx used for parsing double precision dimensions.
   * Decimal separators are taken to be dots, not commas.
   */
  private static final Pattern dimDoubleFormat = Pattern.compile("([0-9.]+?)\\s*?(X|x)\\s*?([0-9.]+?)");
  
  /**
   * Whether or not this Format class shall use double precision (<code>true</code>)
   * or convert all values to integers (<code>false</code>).
   */
  private boolean doublePrecision;
  
  /**
   * Creates a new DimensionFormat for integer dimensions.
   * Equals <code>DimensionFormat(false)</code>.
   */
  public DimensionFormat() {
    // default:
    this(false);
  }
  
  /**
   * Creates a new instance of DimensionFormat with or without double precision.
   *
   * @param dp a boolean value indicating whether or not this DimensionFormat
   * shall represent a double precision dimension.
   */
  public DimensionFormat(boolean dp) {
    super();
    setDoublePrecision(dp); // also sets actuallyUsedFormat
  }
  
  
  /**
   * Formats <code>dim</code> and appends the resulting text to a given string
   * buffer. If <code>pos</code> identifies a field used by the format, its
   * indices are set to the beginning and end of the first such field encountered.
   * If either <code>toAppendTo</code> or <code>pos</code> is <code>null</code>,
   * a <code>NullPointerException</code> is thrown. And if <code>dim</code> isn't
   * a <code>Dimension</code>, an <code>IllegalArgumentException</code> is thrown.
   *
   * @param dim           The dimension object to format
   * @param toAppendTo    where the text is to be appended
   * @param pos    A <code>FieldPosition</code> identifying a field
   *               in the formatted text
   * @return       the string buffer passed in as <code>toAppendTo</code>,
   *               with formatted text appended
   * @exception NullPointerException if <code>toAppendTo</code> or
   *            <code>pos</code> is null
   * @throws    IllegalArgumentException if <code>!(dim instanceof Dimension)</code>
   */
  @Override
  public StringBuffer format(Object dim, StringBuffer toAppendTo, FieldPosition pos) {
    if (toAppendTo == null) {
      throw new NullPointerException("StringBuffer argument cannot be null.");
    }
    if (pos == null) {
      throw new NullPointerException("FieldPosition argument cannot be null.");
    }
    if (dim instanceof Dimension) {
      if (doublePrecision) {
        toAppendTo.append(((Dimension)dim).getWidth());
        toAppendTo.append(" X ");
        toAppendTo.append(((Dimension)dim).getHeight());
      } else {
        toAppendTo.append((int)((Dimension)dim).getWidth());
        toAppendTo.append(" X ");
        toAppendTo.append((int)((Dimension)dim).getHeight());
      }
      return toAppendTo;
    }
    throw new IllegalArgumentException("Cannot format given Object as a Dimension: " + dim.toString());
  }
  
    /**
     * Parses <code>source</code> to create a dimension object.
     * <p>
     * The method attempts to parse <code>source</code> starting at
     * <code>pos</code>. If an error occurs, then the index of 
     * <code>pos</code> is not changed, the error index of 
     * <code>pos</code> is set to the index of the character where 
     * the error occurred, and null is returned.
     *
     * @param source A <code>String</code>, part of which should be parsed.
     * @param pos A <code>ParsePosition</code> object with index and error
     *            index information as described above.
     * @return A <code>Dimension</code> parsed from the string. Since the
     *         abstract class Format defines it as an <code>Object</code>,
     *         however, the Dimension will be returned as an Object. In case 
     *         of error, <code>null</code> is returned.
     * @exception NullPointerException if <code>pos</code> is null.
     */
  @Override
  public Object parseObject(String source, ParsePosition pos) {
    if (pos == null) {
      throw new NullPointerException("FieldPosition argument cannot be null.");
    }
    Matcher m = actuallyUsedFormat.matcher(source);
    if (m.matches()) {
      pos.setIndex(m.end());
      String w = m.group(1);
      String h = m.group(3); // group 2 er X-en i midten
      return doublePrecision ? new Dimension((int)Double.parseDouble(w), (int)Double.parseDouble(h)) :
        new Dimension(Integer.parseInt(w), Integer.parseInt(h));
    }
    
    // if we get here, parsing failed.
    pos.setErrorIndex(-1);
    return null;
  }
  
  /**
   * Tells whether or not this <code>DimensionFormat</code> object
   * permits double precision dimensions.
   *
   * @return a boolean indicating whether or not double precision 
   * dimensions are allowed
   */
  public boolean isDoublePrecision() {
    return doublePrecision;
  }
  
  /**
   * Tells the object to accept or not accept doubles in the dimensions.
   *
   * @param doublePrecision <code>true</code> if doubles are to be accepted,
   * <code>false</code> otherwise
   */
  public void setDoublePrecision(boolean doublePrecision) {
    this.doublePrecision = doublePrecision;
    actuallyUsedFormat = doublePrecision ? dimDoubleFormat : dimIntFormat;
  }
  
}
