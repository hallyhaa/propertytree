/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.babelserver.property.delegate.BooleanDelegate;
import org.babelserver.property.delegate.ColorDelegate;
import org.babelserver.property.delegate.DateDelegate;
import org.babelserver.property.delegate.DimensionDelegate;
import org.babelserver.property.delegate.DropDownDelegate;
import org.babelserver.property.delegate.FileDelegate;
import org.babelserver.property.delegate.PropertyDelegate;
import org.babelserver.property.delegate.TextDelegate;
import org.babelserver.property.tree.PropertyTreeCellEditor;
import org.babelserver.property.tree.PropertyTreeCellRenderer;
import org.babelserver.property.tree.PropertyTreeModel;

/**
 * A PropertyTree is a JTree where the nodes are not only text or images,
 * but all sorts of Swing GUI controls permitting all sorts of values to
 * be represented and interzcted with. A <tt>PropertyTree</tt> may have both
 * {@link Property} objects and «ordinary» <tt>DefaultMutableTreeNodes</tt>
 * objects as nodes.
 * <P>
 * This class along with the Property and PropertyController classes
 * are the only three classes needed to take advantage of a fully
 * functional PropertyTree. For a tree with only one node, this
 * would be the code needed:
 * <PRE>
 * PropertyController pc = new PropertyController();
 * Property root         = new Property("I like bananas", true, pc);
 * PropertyTree pt       = new PropertyTree(root);
 * </PRE>
 * Put it into a JPanel and show it in a JFrame. The boolean value
 * will be toggleable and <code>pc</code> will be notified every
 * time the value is changed.
 *
 * @author  Hallvard Ystad
 */
public class PropertyTree extends JTree {
  
  /** Transient in super class, but we'd like to store it. */
  protected TreeModel        treeModel;
  
  /** 
   * Maps objects to delegates. Used for finding the right delegate 
   * for cell renderers and editors.
   */
  private HashMap<Class<?>, Class<? extends PropertyDelegate>> delegateMap;

  /**
   * Creates new PropertyTree. The empty constructor creates a tree
   * containing Property objects of all sorts but <code>Color</code>s.
   * Having a colour chooser inside a JTree takes much space and is 
   * not recommendable. The <code>Property</code> objects in this 
   * default tree are not connected to any <code>PropertyController</code>, 
   * and nothing happens as they are changed.
   */
  public PropertyTree() {
    this(getDefaultTreeModel(), true);
  }

//    private PropertyTree(Object[] value) throws Exception {
//      throw new Exception("Don't use this constructor!");
//    }
  
//    private PropertyTree(Vector<?> value) throws Exception {
//      throw new Exception("Don't use this constructor!");
//    }
  
//    private PropertyTree(Hashtable<?,?> value) throws Exception {
//      throw new Exception("Don't use this constructor!");
//    }
  
  /**
   * Creates new PropertyTree with the specified root node. In the
   * super class JTree, the corresponding constructor demands a
   * <code>TreeNode</code>, but we are a bit more specific. We need
   * a <code>Property</code>.
   * <P>
   * This constructor is shorthand for
   * <code>PropertyTree(new PropertyTreeModel(root), true)</code>
   *
   * @param root the root Property object.
   */
  public PropertyTree(Property root) {
    this(new PropertyTreeModel(root), true);
  }
  
  /**
   * Creates new PropertyTree with the specified TreeModel.
   * <P>
   * If <code>alignChildNodes</code> is set to <code>true</code>,
   * <code>Property</code> object values will be vertically aligned within
   * each tree path.
   * <P>
   * Both other constructors delegate to this one.
   *
   * @param model the PropertyTreeModel object.
   * @param alignChildNodes whether or not to vertically align
   * <code>Property</code> object values within tree paths.
   */
  public PropertyTree(TreeModel model, boolean alignChildNodes) {
    super(model);
    // The following lines are in the super class constructor
    // (and therefore commented out here):
    /*
    //expandedStack = new Stack();  // private access in JTree
    toggleClickCount = 2;
    //expandedState = new Hashtable();  // private access in JTree
    //setLayout(null);
    rowHeight = 16;
    visibleRowCount = 20;
    rootVisible = true;
    selectionModel = new DefaultTreeSelectionModel();
    cellRenderer = null;
    scrollsOnExpand = true;
    setOpaque(true);
    //expandsSelectedPaths = true;  // private access in JTree
     */
    
    // our own lines:
    getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    setCellRenderer(new PropertyTreeCellRenderer());
    setEditable(true);
    setCellEditor(new PropertyTreeCellEditor((PropertyTreeCellRenderer) getCellRenderer()));

    // populate the delegateMap:
    delegateMap = new HashMap<>();
    delegateMap.put(Boolean.class, BooleanDelegate.class);
    delegateMap.put(Color.class, ColorDelegate.class);
    delegateMap.put(Date.class, DateDelegate.class);
    delegateMap.put(Dimension.class, DimensionDelegate.class);
    delegateMap.put(ComboBoxModel.class, DropDownDelegate.class);
    delegateMap.put(DefaultComboBoxModel.class, DropDownDelegate.class);
    delegateMap.put(Object[].class, DropDownDelegate.class);
    delegateMap.put(File.class, FileDelegate.class);
    delegateMap.put(CharSequence.class, TextDelegate.class);
    delegateMap.put(String.class, TextDelegate.class);
    
    // From super() again, but we keep these,
    // since we have modified:
    updateUI();
    setModel(model);
    
    // then ours again:
    if (alignChildNodes) {
      addAlignListener();
    }
  }

  /**
   * Description copied (and modified) from JTree: Creates and returns a sample
   * <tt>PropertyTreeModel</tt>. Used primarily for beanbuilders to show
   * something interesting.
   *
   * @return the default <tt>PropertyTreeModel</tt>
   */
  protected static DefaultTreeModel getDefaultTreeModel() {
    Property root = new Property("", "PropertyTree");
    Property parent;
    
    parent = new Property("", "Names (Strings)");
    parent.add(new Property("First name", "John"));
    parent.add(new Property("Family name", "Doe"));
    root.add(parent);
    
    parent = new Property("", "Opinions (booleans)");
    parent.add(new Property("I like this tree", true));
    parent.add(new Property("I dislike this tree", false));
    root.add(parent);
    
    parent = new Property("", "Colours");
    parent.add(new Property("Red", Color.RED));
    parent.add(new Property("Blue", Color.BLUE));
    parent.add(new Property("Green", Color.GREEN));
    root.add(parent);
    
    parent = new Property("", "Screen resolutions (Dimensions)");
    parent.add(new Property("Small", new Dimension(1280, 1024)));
    parent.add(new Property("Medium", new Dimension(1920, 1080)));
    parent.add(new Property("Big", new Dimension(3840, 2160)));
    root.add(parent);
    
    parent = new Property("", "Fortune (ComboBoxes)");
    String[] fortune;
    fortune = new String[] {"Put no trust in cryptic comments", "She's genuinely bogus", "Optimization hinders evolution"};
    parent.add(new Property("Short ones", fortune));
    fortune = new String[] {"The only problem with being a man of leisure is that you can never stop and take a rest",
                            "New members are urgently needed in the Society for Prevention of Cruelty to Yourself. Apply within.",
                            "Always borrow money from a pessimist; he doesn't expect to be paid back"};
    parent.add(new Property("Longer ones", fortune));
    root.add(parent);
    
    parent = new Property("", "Ordinary strings");
    parent.add(new Property("Ordinary property", "You may change this text"));
    parent.add(new Property("", "«One string editable» (like above, but with no description part)"));
    //parent.add(new Property("«One string editable» with no callback", "")); // like below. Easier to use DefaultMutableTreeNode, I believe
    parent.add(new DefaultMutableTreeNode("DefaultMutableTreeNode - changeable, but with no callback"));
    root.add(parent);

    parent = new Property("", "Constitutions (Dates)");
    Calendar cal = Calendar.getInstance(); // uses default locale
    cal.set(1958, 9, 4);
    parent.add(new Property("France", cal.getTime()));
    cal.set(1814, 4, 17);
    parent.add(new Property("Norway", cal.getTime()));
    cal.set(1787, 6, 17);
    parent.add(new Property("USA", cal.getTime()));
    root.add(parent);
    
    parent = new Property("", "Files");
    File file = new File("/probably/doesn't/exist");
    parent.add(new Property("Inexistant", file));
    file = new File("/");
    parent.add(new Property("File System Root", file));
    root.add(parent);

    /* TODO / Possible enhancements:
      -- radio button choosers (within a TreePath) 
     */

    return new DefaultTreeModel(root);
  }
  
  /**
   * Adds a TreeExpansionListener to the PropertyTree.
   * This method is called whenever one decides to have
   * all <code>Property</code> object values vertically
   * aligned in the tree.
   */
  private void addAlignListener() {
    addTreeExpansionListener(new TreeExpansionListener(){
      @Override
      public void treeExpanded(TreeExpansionEvent event) {
        // Source is the PropertyTree
        alignNodeValues(event.getPath());
      }
      @Override
      public void treeCollapsed(TreeExpansionEvent event) {
        // no worries!
      }});
  }
  
  /**
   * Aligns values in all nodes underneath {@code path}. Description labels will be 
   * set to the same size, thus making all {@code Property} values appear vertically 
   * aligned. The process is recursive, so if a node contains other non-leaf nodes, 
   * they will also have their children «value-aligned».
   * <P>
   * Since the description JLabels in {@code Property} object have no width before 
   * they are painted on screen, this method will have no effect if called before 
   * the labels are laid out.
   * <P>
   * Perform this method on the root node to align all {@code Property} values in 
   * all the different branches of the tree.
   * <P>
   * <B>Caution: This has no effect on nodes that haven't yet been painted.</B>
   * 
   * //TODO: Funker dette når vi ikke kontrollerer JLabelene lenger??
   *
   * @param path the <code>TreePath</code> whose children are to be aligned.
   * Nothing happens on paths pointing to leaf nodes.
   */
  public void alignNodeValues(TreePath path) {
    /*
    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
    int descriptionWidth = 0, delegateWidth = 0;
    Vector<Integer> changes = new Vector<Integer>();
    // first pass, obtaining the right values:
    for (int i=0; i<getModel().getChildCount(node); i++) {
      TreeNode each = node.getChildAt(i);
      if (each instanceof Property) {
        Property child = (Property) each;
        if (child.getUserObject() != null) {
          //public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
          Component c = getCellRenderer().getTreeCellRendererComponent(this, child,
                                                                       getSelectionPath().getLastPathComponent().equals(each),
                                                                       isExpanded(new TreePath(each)),
                                                                       each.isLeaf(),
                                                                       getRowForPath(new TreePath(each)),
                                                                       true); // let's just compare all as if they are in focus
          int descWidth = c.getWidth();
          descriptionWidth = Math.max(descriptionWidth, descWidth);
        
          int delWidth = child.getDelegate().getPreferredSize().width;
          delegateWidth = Math.max(delegateWidth, delWidth);
        }
      }
    }
    
    // Second pass, setting the values, assuring recursion
    for (int i=0; i<getModel().getChildCount(node); i++) {
      TreeNode each = node.getChildAt(i);
      if (each instanceof Property) {
        Property child = (Property) each;
      
        if (child.getUserObject() != null) {
        
          // all description labels same size:
          child.getDescriptionLabel().setPreferredSize(new Dimension(descriptionWidth, child.getDescriptionLabel().getPreferredSize().height));
        
          // then the delegate:
          child.getDelegate().setPreferredSize(new Dimension(delegateWidth, child.getDelegate().getPreferredSize().height));
        
          // record this node's index for update:
          changes.add(i);
          // the panel should adjust itself accordingly
        
        } else {
            alignNodeValues(path.pathByAddingChild(child));
        }
      }
    }
    
    // repaint the cells:
    
    // first convert the Vector to an array:
    int[] iChanges = new int[changes.size()];
    for (int i=0; i<changes.size(); i++) {
      iChanges[i] = changes.elementAt(i);
    }
    
    // then fire necessary events, so that the path is repainted:
    ((DefaultTreeModel)getModel()).nodesChanged(node, iChanges);
    */
  }
  
  /////////////////////////////////////
  //
  // Methods concerning the delegateMap (rest of file)
  //
  /////////////////////////////////////

  /**
   * Tries to register a new PropertyDelegate for an object.
   * The delegate must implement the {@link PropertyDelegate}
   * interface and must supply a one-parameter constructor for
   * automatic instantiation.
   * 
   * @since 4.1
   * 
   * @param objectType
   * @param delegateClass
   * @return <tt>delegateMap.put(objectType, delegateClass)</tt>
   * @throws java.lang.NoSuchMethodException if <tt>delegateClass</tt> doesn't
   *         provide a one-parameter constructor accepting only a <tt>objectType</tt>
   *         type object.
   */
  public Object registerDelegate(Class<?> objectType, Class<? extends PropertyDelegate> delegateClass) throws NoSuchMethodException {
    boolean constructorOK = false;
    for (Constructor<?> c : delegateClass.getConstructors()) {
      if ((c.getParameterTypes().length == 1) && c.getParameterTypes()[0].isAssignableFrom(objectType)) {
        constructorOK = true;
        break;
      }
    }
    if (!constructorOK) {
      throw new NoSuchMethodException("Unable to find a constructor for " + delegateClass +
                                      " with " + objectType + " as only parameter.");
    }
    return delegateMap.put(objectType, delegateClass);
  }
  
  
  /**
   * Gets the Delegate class for dealing with the <tt>Property</tt>. This
   * class first peeks into the {@link #delegateMap} to look up
   * <tt>Property.getUserObject().getClass()</tt> as a key. If this doesn't
   * yield anything, we check if <tt>Property.getUserObject().getClass()</tt>
   * could be an instance of (e.g. by representing a subclass) some class
   * in the <tt>delegateMap</tt> anyway.
   * <p>
   * This method throws a {@link NoSuchMethodException} if we try to retrieve
   * a delegate that doesnt offer a constructor taking one single parameter,
   * and that parameter, of course, has to be an instance of <tt>prop</tt>'s
   * <tt>userObject</tt>.
   * 
   * @return a delegate to deal with the passed in Property. This method never
   *         returns <tt>null</tt>. If we have no information about what delegate
   *         to use, we return the {@link TextDelegate}.
   * @since 4.1
   * 
   * @param prop
   * @throws java.lang.NoSuchMethodException
   */
  public PropertyDelegate getDelegate(Property prop) throws NoSuchMethodException {
    Class<? extends PropertyDelegate> delegateClass = delegateMap.get(prop.getUserObject().getClass());
    if (delegateClass == null) {
      delegateClass = getDelegateAnyway(prop);
    }
    Constructor<? extends PropertyDelegate> constructor = null;
    for (Constructor<?> c : delegateClass.getConstructors()) {
      if ((c.getParameterTypes().length == 1) && c.getParameterTypes()[0].isAssignableFrom(prop.getUserObject().getClass())) {
        constructor = (Constructor<? extends PropertyDelegate>) c;
      }
    }
    if (constructor == null) {
      throw new NoSuchMethodException("Unable to find a constructor for " + delegateClass.getClass() +
                                      " with " + prop.getUserObject().getClass() + " as only parameter.");
    }
    try {
      return constructor.newInstance(prop.getUserObject());
    } catch (InstantiationException |
             IllegalAccessException |
             IllegalArgumentException |
             InvocationTargetException ex) {
      throw new NoSuchMethodException(ex.getMessage());
    }
  }

  /**
   * Tries to get an adequate delegate for <tt>prop.getUserObject()</tt> by
   * checking its hierarchy of superclasses. If this does not help, a
   * <tt>TextDelegate</tt> will be returned. This method is the «non-lazy» part
   * of {@link #getDelegate(org.babelserver.property.Property) getDelegate(prop)}.
   * 
   * @since 4.1
   *
   * @return a delegate. If none is found, we return {@link TextDelegate}.
   */
  private Class<? extends PropertyDelegate> getDelegateAnyway(Property prop) {
    
    // check this common case first:
    if (prop.getUserObject() instanceof CharSequence) {
      return delegateMap.get(prop.getUserObject().getClass());
    }

    // Check superclasses:
    Set<Class<?>> registeredClasses = delegateMap.keySet();
    for (Class<?> c : registeredClasses) {
      if (c.isInstance(prop.getUserObject())) {
        return delegateMap.get(c);
      } 
    }

    // if everything else fails, return the default class:
    return delegateMap.get(CharSequence.class);
  }

}
