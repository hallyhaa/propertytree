/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import org.babelserver.property.delegate.BooleanDelegate;
import org.babelserver.property.delegate.ColorDelegate;
import org.babelserver.property.delegate.DateDelegate;
import org.babelserver.property.delegate.DimensionDelegate;
import org.babelserver.property.delegate.DropDownDelegate;
import org.babelserver.property.delegate.FileDelegate;
import org.babelserver.property.delegate.PropertyDelegate;
import org.babelserver.property.delegate.TextDelegate;
import org.babelserver.property.list.ListCellEditor;
import org.babelserver.property.list.PropertyListCellEditor;
import org.babelserver.property.list.PropertyListCellRenderer;
import org.babelserver.property.list.PropertyListModel;

/**
 * A PropertyList is a customized JList where the nodes are not only
 * JLabels, but all sorts of Swing GUI controls permitting special
 * values to be altered and changed. The only type of nodes permitted
 * in a PropertyList, are <code>Property</code> objects.
 * <P>
 * This class along with the {@code Property}, {@code PropertyListModel}
 * and {@code PropertyController} classes constitute the only four
 * classes one needs to use in order to take advantage of a fully
 * functional PropertyList. Having a list with only one node, this would
 * be the code needed:
 * <PRE>
 * PropertyController pc = new PropertyController();
 * Property bananas      = new Property("I like bananas", true, pc);
 * PropertyList pl       = new PropertyList();
 * PropertyListModel mod = (PropertyListModel) pl.getModel();
 * mod.addElement(bananas);
 * </PRE>
 * Put it into a JPanel and show it in a JFrame. The boolean value
 * will be toggleable and <code>pc</code> will be notified every
 * time the value is changed.
 *
 * @since 2.0
 * @author  Hallvard Ystad
 */
public class PropertyList<E> extends JList<E> implements CellEditorListener {

  /** Bound property name for <tt>cellEditor</tt>. */
  public static final String CELL_EDITOR_PROPERTY = "cellEditor";
  private PropertyListCellEditor cellEditor = null;
  private Component editorComp = null;
  private PropertyChangeListener editorRemover = null;

  /** Identifies the index of the cell being edited. */
  protected transient int editingIndex = -1;

  /** 
   * Maps objects to delegates. Used for finding the right delegate 
   * for cell renderers and editors.
   */
  private HashMap<Class<?>, Class<? extends PropertyDelegate>> delegateMap;

  /**
   * Constructs a <code>PropertyList</code> with a model showing
   * the different possible uses of <code>PropertyList</code>s.
   */
  public PropertyList() {
    this(getDefaultListModel());
  }

  /**
   * Constructs a {@code PropertyList} that displays elements from the specified
   * non-null model. All {@code PropertyList} constructors delegate to this one.
   *
   * @param model the model for the list
   * @exception IllegalArgumentException if the model is {@code null}
   */
  public PropertyList(ListModel<E> model) {
    super(model);
    setCellRenderer(new PropertyListCellRenderer());
    setCellEditor(new PropertyListCellEditor());

    // populate the delegateMap:
    delegateMap = new HashMap<>();
    delegateMap.put(Boolean.class, BooleanDelegate.class);
    delegateMap.put(Color.class, ColorDelegate.class);
    delegateMap.put(Date.class, DateDelegate.class);
    delegateMap.put(Dimension.class, DimensionDelegate.class);
    delegateMap.put(ComboBoxModel.class, DropDownDelegate.class);
    delegateMap.put(DefaultComboBoxModel.class, DropDownDelegate.class);
    delegateMap.put(Object[].class, DropDownDelegate.class);
    delegateMap.put(File.class, FileDelegate.class);
    delegateMap.put(CharSequence.class, TextDelegate.class);
    delegateMap.put(String.class, TextDelegate.class);
    
    addMouseListener(new MouseHandler());
  }

  /**
   * Sets the cell editor.  A <code>null</code> value implies that the
   * list cannot be edited.  If this represents a change in the
   * <code>cellEditor</code>, the <code>propertyChange</code>
   * method is invoked on all listeners.
   *
   * @param cellEditor the <code>ListCellEditor</code> to use
   */
  public void setCellEditor(PropertyListCellEditor cellEditor) {
    PropertyListCellEditor oldEditor = this.cellEditor;

    this.cellEditor = cellEditor;
    firePropertyChange(CELL_EDITOR_PROPERTY, oldEditor, cellEditor);
    invalidate();
  }

  /**
   * Returns the {@code cellEditor} component.
   * @return the {@code cellEditor} component
   */
  public PropertyListCellEditor getCellEditor() {
    return cellEditor;
  }

  /**
   * Returns the component being used for edits in the PropertyList.
   *
   * @return the editor component
   */
  public Component getEditorComponent() {
    return editorComp;
  }

  /**
   * Returns the {@code ListCellEditor} component associated with
   * this PropertyList.
   *
   * @return the {@code ListCellEditor}
   */
  public ListCellEditor getListCellEditor() {
    return cellEditor;
  }

  //
  // Implementing the CellEditorListener interface
  //
  /**
   * Invoked when editing is finished.
   *
   * @param e the ChangeEvent
   */
  @Override
  public void editingStopped(ChangeEvent e) {
    // Take in the new value
    if (cellEditor != null) {
      Object value = cellEditor.getCellEditorValue();
      if (!((getModel()).getElementAt(editingIndex) instanceof Property)) {
        ((DefaultListModel) getModel()).setElementAt(value, editingIndex); // This only works for Â«ordinaryÂ» cells
      } else {
        ((Property) getModel().getElementAt(editingIndex)).setUserObject(value);
      }

      removeEditor();
    }
  }

  /**
   * Invoked when editing is canceled. The editor object is discarded
   * and the cell is rendered once again.
   * <p>
   * Application code will not use these methods explicitly, they
   * are used internally by PropertyList.
   *
   * @param e the event received
   */
  @Override
  public void editingCanceled(ChangeEvent e) {
    removeEditor();
  }

  /**
   * Tells whether or not a cell in the PropertyList is
   * in the midst of being edited.
   *
   * @return {@code true} if {@link  #editorComp  editorComp}
   * isn't {@code null}, {@code false} otherwise.
   */
  public boolean isEditing() {
    return editorComp != null;
  }


  /**
   * Prepares the editor by querying the data model for the value and
   * selection state of the cell at <code>index</code>. This method is
   * more or less copied from that of JTable, and shuold therefore
   * probably be used in a similar manner.
   *
   * @param listCellEditor the {@code ListCellEditor} to set up
   * @param index          the index of the cell to edit
   * @return the {@code Component} being edited
   */
  public Component prepareEditor(ListCellEditor listCellEditor, int index) {
    Object value = getModel().getElementAt(index);
    boolean isSelected = isSelectedIndex(index); // silly name for a method, really!
    boolean cellHasFocus = hasFocus() && (index == getLeadSelectionIndex());
    Component comp = listCellEditor.getListCellEditorComponent(this, value, index, isSelected, cellHasFocus);
    // if we use the Property package properly, comp is now a JPanel or a JLabel.
    // TODO: look for not-depreciated replacement for code underneath, if needed
    // Why this? Really irritating when Sun uses depreciated ways of doing things...
    if (comp instanceof JComponent) {
      JComponent jComp = (JComponent) comp;
      if (jComp.getNextFocusableComponent() == null) {
        jComp.setNextFocusableComponent(this);
      }
    }
    return comp;
  }

  /**
   * Discards the editor object and frees the real estate it used for
   * cell rendering.
   */
  public void removeEditor() {
    KeyboardFocusManager.getCurrentKeyboardFocusManager().removePropertyChangeListener("permanentFocusOwner", editorRemover);
    editorRemover = null;

    if (cellEditor != null) {
      cellEditor.removeCellEditorListener(this);
      if (editorComp != null) {
        Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        boolean isFocusOwnerInTheList = focusOwner != null ? SwingUtilities.isDescendingFrom(focusOwner, this) : false;
        remove(editorComp);
        if (isFocusOwnerInTheList) {
          requestFocusInWindow();
        }
      }

      Rectangle cellRect = getCellBounds(editingIndex, editingIndex);

      //setCellEditor(null); // this works in JTable, so why not here?
      setEditingIndex(-1);
      editorComp = null;

      repaint(cellRect);
    }
  }

  /**
   * Sets the <code>editingIndex</code> variable.
   * @param editingIndex  the column of the cell to be edited
   *
   * @see #editingIndex
   */
  public void setEditingIndex(int editingIndex) {
    this.editingIndex = editingIndex;
  }

  /**
   * Returns the <code>editingIndex</code> variable.
   *
   * @return an integer indicating which cell in the list
   * is being edited. Returns -1 if none is.
   */
  public int getEditingIndex() {
    return editingIndex;
  }

  /**
   * Programmatically starts editing the cell at <code>index</code>,
   * if that index is in the valid range, and the cell is editable.
   * To prevent the <code>PropertyList</code> from editing a particular
   * list or cell value, return <code>false</code> from the
   * <code>isCellEditable</code> method in the <code>PropertyListModel</code>.
   * Default behaviour is to return <code>false</code> for Property cells
   * that are pure description, and <code>true</code> only if the
   * <code>Property</code> object has a delegate object.
   *
   * @param   index   the index to be edited
   * @param   e       event to pass into <code>shouldSelectCell</code>;
   *                  note that as of Java 2 platform v1.2, the call to
   *                  <code>shouldSelectCell</code> is no longer made
   * @return  false if for any reason the cell cannot be edited,
   *                or if the indices are invalid
   */
  public boolean editCellAt(int index, EventObject e) {
    if (cellEditor != null && !cellEditor.stopCellEditing()) {
      return false;
    }

    if (index < 0 || index >= getModel().getSize()) {
      return false;
    }

    if (editorRemover == null) {
      KeyboardFocusManager fm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
      editorRemover = new CellEditorRemover(fm);
      fm.addPropertyChangeListener("permanentFocusOwner", editorRemover);
    }

    if (cellEditor != null && cellEditor.isCellEditable(e)) {
      editorComp = prepareEditor(cellEditor, index);
      if (editorComp == null) {
        removeEditor();
        return false;
      }
      editorComp.setBounds(getCellBounds(index, index));
      ((JComponent) editorComp).setOpaque(true); // TODO: look into this
      add(editorComp);
      editorComp.validate();
      //editorComp.repaint(); // from JTable, but not needed, or at least so it seems
      setCellEditor(cellEditor); // OK in JTable, but seems not here
      setEditingIndex(index);
      cellEditor.addCellEditorListener(this);

      return true;
    }
    return false;
  }

  /**
   * Removes the PropertyChangeListener from the {@code editorRemover}.
   */
  @Override
  public void removeNotify() {
    KeyboardFocusManager.getCurrentKeyboardFocusManager().removePropertyChangeListener("permanentFocusOwner", editorRemover);
    //editorRemover = null;
    super.removeNotify();
  }

  /**
   * Constructs a <code>PropertyListModel</code> from an array of objects,
   * and calls {@code setModel} with this model.
   * <P>
   * Attempts to pass a {@code null} value to this method results in
   * undefined behavior and, most likely, exceptions. The created model
   * references the given array directly. Attempts to modify the array
   * after invoking this method results in undefined behavior.
   * <P>
   * This method is overridden solely to make sure the
   * model used is an instance of {@code  PropertyListModel}.
   *
   * @param listData {@inheritDoc}
   */
  @Override
  public void setListData(final Object[] listData) {
    setModel(new PropertyListModel() {

      @Override
      public int getSize() {
        return listData.length;
      }

      @Override
      public Object getElementAt(int i) {
        return listData[i];
      }
    });
  }

  /**
   * Constructs a <code>PropertyListModel</code> from a Vector,
   * and calls {@code setModel} with this model.
   * <P>
   * Attempts to pass a {@code null} value to this method results in
   * undefined behavior and, most likely, exceptions. The created model
   * references the given {@code Vector} directly. Attempts to modify the
   * {@code Vector} after invoking this method results in undefined behavior.
   * <P>
   * This method is overridden solely to make sure the
   * model used is an instance of {@code  PropertyListModel}.
   *
   * @param listData {@inheritDoc}
   */
  @Override
  public void setListData(final Vector<? extends E> listData) {
    setModel(new PropertyListModel() {

      @Override
      public int getSize() {
        return listData.size();
      }

      @Override
      public Object getElementAt(int i) {
        return listData.elementAt(i);
      }
    });
  }

  // <editor-fold defaultstate="collapsed" desc=" getDefaultListModel ">
  /**
   * Dummy method to create a DefaultListModel. This is needed
   * mostly for graphical IDEs.
   *
   * @return a list model showing the different possible
   * Property types.
   */
  private static DefaultListModel getDefaultListModel() {
    DefaultListModel model = new DefaultListModel();

    DefaultMutableTreeNode prop = new DefaultMutableTreeNode("PropertyList");
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Names (Strings)");
    model.addElement(prop);
    prop = new Property("First name", "John");
    model.addElement(prop);
    prop = new Property("Family name", "Doe");
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Opinions (booleans)");
    model.addElement(prop);
    prop = new Property("I like this list", true);
    model.addElement(prop);
    prop = new Property("I dislike this list", false);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Colours");
    model.addElement(prop);
    prop = new Property("Red", Color.RED);
    model.addElement(prop);
    prop = new Property("Blue", Color.BLUE);
    model.addElement(prop);
    prop = new Property("Green", Color.GREEN);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Screen resolutions (Dimensions)");
    model.addElement(prop);
    prop = new Property("Small", new Dimension(1280, 1024));
    model.addElement(prop);
    prop = new Property("Medium", new Dimension(1920, 1080));
    model.addElement(prop);
    prop = new Property("Big", new Dimension(3840, 2160));
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Fortune (ComboBoxes)");
    model.addElement(prop);
    String[] fortune;
    fortune = new String[]{"Put no trust in cryptic comments", "She's genuinely bogus", "Optimization hinders evolution"};
    prop = new Property("Short ones", fortune);
    model.addElement(prop);
    fortune = new String[]{"The only problem with being a man of leisure is that you can never stop and take a rest", "New members are urgently needed in the Society for Prevention of Cruelty to Yourself. Apply within.", "Always borrow money from a pessimist; he doesn't expect to be paid back"};
    prop = new Property("Longer ones", fortune);
    model.addElement(prop);

    // The tree has this:
    //parent = new Property("", "Ordinary strings");
    //parent.add(new Property("Ordinary property", "You may change this text"));
    //parent.add(new Property("", "«One string editable» (like above, but with no description part)"));
    ////parent.add(new Property("«One string editable» with no callback", "")); // like below. Easier to use DefaultMutableTreeNode, I believe
    //parent.add(new DefaultMutableTreeNode("DefaultMutableTreeNode - changeable, but with no callback"));
    //root.add(parent);

    prop = new DefaultMutableTreeNode("Constitutions (Dates)");
    model.addElement(prop);
    Calendar cal = Calendar.getInstance(); // uses default locale
    cal.set(1958, 9, 4);
    prop = new Property("France", cal.getTime());
    model.addElement(prop);
    cal.set(1814, 4, 17);
    prop = new Property("Norway", cal.getTime());
    model.addElement(prop);
    cal.set(1787, 6, 17);
    prop = new Property("USA", cal.getTime());
    model.addElement(prop);

    prop = new DefaultMutableTreeNode("Files");
    model.addElement(prop);
    File file = new File("/probably/doesn't/exist");
    prop = new Property("Inexistant", file);
    model.addElement(prop);
    file = new File("/");
    prop = new Property("File System Root", file);
    model.addElement(prop);

    /* @TODO / Possible enhancements:
    -- radio button choosers
     */

    return model;
  } // </editor-fold>

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
  private void initComponents() {
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  // End of variables declaration//GEN-END:variables
  /**
   * This class tracks changes in the keyboard focus state. It is used
   * when the PropertyList is editing to determine when to cancel the edit.
   * If focus switches to a component outside of the PropertyList, but in the
   * same window, this will cancel editing.
   * <P>
   * This inner class is copied with almost no changes from the {@code JTable}
   * class. Funny name for a class, by the way.
   */
  class CellEditorRemover implements PropertyChangeListener {

    /**
     * We obviously need a focusManager to do what we are supposed
     * to in this class. The question is, why is it sent as a
     * parameter to the constructor? Why don't we just call
     * {@code KeyboardFocusManager.getCurrentKeyboardFocusManager()}?
     */
    /**
     * We obviously need a focusManager to do what we are supposed
     * to in this class. The question is, why is it sent as a
     * parameter to the constructor? Why don't we just call
     * {@code KeyboardFocusManager.getCurrentKeyboardFocusManager()}?
     */
    KeyboardFocusManager focusManager;

    /**
     * Constructs the object and sets the KeyboardFocusManager.
     *
     * @param fm the new KeyboardFocusManager
     */
    public CellEditorRemover(KeyboardFocusManager fm) {
      this.focusManager = fm;
    }

    /**
     * This method is called when a bound property is changed. If the
     * PropertyList looses focus, {@link javax.swing.CellEditor#stopCellEditing()
     * stopCellEditing()} and if necessary
     * {@link javax.swing.CellEditor#cancelCellEditing() cancelCellEditing()}
     * will be called.
     *
     * @param change A PropertyChangeEvent object describing the
     * event source and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent change) {
      if (!isEditing() || getClientProperty("terminateEditOnFocusLost") != Boolean.TRUE) {
        return;
      }

      Component c = focusManager.getPermanentFocusOwner();
      while (c != null) {
        if (c == PropertyList.this) {
          // focus remains inside the PropertyList
          return;
        } else if ((c instanceof Window) || (c instanceof Applet && c.getParent() == null)) {
          if (c == SwingUtilities.getRoot(PropertyList.this)) {
            if (!getListCellEditor().stopCellEditing()) {
              getListCellEditor().cancelCellEditing();
            }
          }
          break;
        }
        c = c.getParent();
      }
    }
  }

/**
   * Handles mouse clicks. This really should be a stripped down
   * version of the {@code Handler} inner class from the
   * {@code BasicTableUI} class, but I found it already stripped
   * on http://jroller.com/page/santhosh?entry=making_jlist_editable_no_jtable.
   */
  private class MouseHandler extends MouseAdapter {

    /**
     * Component receiving mouse events during editing.
     * May not be editorComponent.
     */
    private Component dispatchComponent;
    private boolean selectedOnPress;

    private void setDispatchComponent(MouseEvent e) {
      Component editorComponent = getEditorComponent();
      Point p = e.getPoint();
      Point p2 = SwingUtilities.convertPoint(PropertyList.this, p, editorComponent);
      dispatchComponent = SwingUtilities.getDeepestComponentAt(editorComponent, p2.x, p2.y);
    }

    private boolean repostEvent(MouseEvent e) {
      // Check for isEditing() in case another event has
      // caused the editor to be removed. See bug #4306499.
      if (dispatchComponent == null || !isEditing()) {
        return false;
      }
      MouseEvent e2 = SwingUtilities.convertMouseEvent(PropertyList.this, e, dispatchComponent);
      dispatchComponent.dispatchEvent(e2);
      return true;
    }

    /**
     * Seems to replace a method with the same name
     * in sun.swing.SwingUtilities2. That's native code.
     *
     * @param e {@inheritDoc}
     * @return {@inheritDoc}
     */
    private boolean shouldIgnore(MouseEvent e) {
      return e.isConsumed() || (!(SwingUtilities.isLeftMouseButton(e) && isEnabled()));
    }

    /**
     * {@inheritDoc}
     *
     * @param e {@inheritDoc}
     */
    @Override
    public void mousePressed(MouseEvent e) {
      if (e.isConsumed()) {
        selectedOnPress = false;
        return;
      }
      selectedOnPress = true;
      adjustFocusAndSelection(e);
    }

    void adjustFocusAndSelection(MouseEvent e) {
      if (shouldIgnore(e)) {
        return;
      }

      Point p = e.getPoint();
      int index = locationToIndex(p);
      // The autoscroller can generate drag events outside the Table's range.
      if (index == -1) {
        return;
      }

      if (editCellAt(index, e)) {
        setDispatchComponent(e);
        repostEvent(e);
        PropertyList list = (PropertyList) e.getSource();
        Object cell = list.getModel().getElementAt(index);
        Component editorComp = list.getEditorComponent();
        if (editorComp != null) {
          editorComp.requestFocus();
          if (editorComp instanceof JPanel && ((JPanel) editorComp).getComponentCount() > 1) {
            // chances are, it's a Property being edited...
            ((JPanel) editorComp).getComponent(1).requestFocus();
            // TODO: shouldn't be hard coded as no 1
            // RIGHT! crashes with colour choosing
          }
        }
      } else {
        if (isRequestFocusEnabled()) {
          requestFocus();
        }
      }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(MouseEvent e) {
      if (selectedOnPress) {
        if (shouldIgnore(e)) {
          return;
        }

        repostEvent(e);
        dispatchComponent = null;
        setValueIsAdjusting(false);
      } else {
        adjustFocusAndSelection(e);
      }
    }
  }

  /////////////////////////////////////
  //
  // Methods concerning the delegateMap (rest of file)
  //
  /////////////////////////////////////

  /**
   * Tries to register a new PropertyDelegate for an object.
   * The delegate must implement the {@link PropertyDelegate}
   * interface and must supply a one-parameter constructor for
   * automatic instantiation.
   * 
   * @since 4.1
   * 
   * @param objectType
   * @param delegateClass
   * @return <tt>delegateMap.put(objectType, delegateClass)</tt>
   * @throws java.lang.NoSuchMethodException if <tt>delegateClass</tt> doesn't
   *         provide a one-parameter constructor accepting only a <tt>objectType</tt>
   *         type object.
   */
  public Object registerDelegate(Class<?> objectType, Class<? extends PropertyDelegate> delegateClass) throws NoSuchMethodException {
    boolean constructorOK = false;
    for (Constructor<?> c : delegateClass.getConstructors()) {
      if ((c.getParameterTypes().length == 1) && c.getParameterTypes()[0].isAssignableFrom(objectType)) {
        constructorOK = true;
        break;
      }
    }
    if (!constructorOK) {
      throw new NoSuchMethodException("Unable to find a constructor for " + delegateClass +
                                      " with " + objectType + " as only parameter.");
    }
    return delegateMap.put(objectType, delegateClass);
  }
  
  
  /**
   * Gets the Delegate class for dealing with the <tt>Property</tt>. This
   * class first peeks into the {@link #delegateMap} to look up
   * <tt>Property.getUserObject().getClass()</tt> as a key. If this doesn't
   * yield anything, we check if <tt>Property.getUserObject().getClass()</tt>
   * could be an instance of (e.g. by representing a subclass) some class
   * in the <tt>delegateMap</tt> anyway.
   * <p>
   * This method throws a {@link NoSuchMethodException} if we try to retrieve
   * a delegate that doesnt offer a constructor taking one single parameter,
   * and that parameter, of course, has to be an instance of <tt>prop</tt>'s
   * <tt>userObject</tt>.
   * 
   * @return a delegate to deal with the passed in Property. This method never
   *         returns <tt>null</tt>. If we have no information about what delegate
   *         to use, we return the {@link TextDelegate}.
   * @since 4.1
   * 
   * @param prop
   * @throws java.lang.NoSuchMethodException
   */
  public PropertyDelegate getDelegate(Property prop) throws NoSuchMethodException {
    Class<? extends PropertyDelegate> delegateClass = delegateMap.get(prop.getUserObject().getClass());
    if (delegateClass == null) {
      delegateClass = getDelegateAnyway(prop);
    }
    Constructor<? extends PropertyDelegate> constructor = null;
    for (Constructor<?> c : delegateClass.getConstructors()) {
      if ((c.getParameterTypes().length == 1) && c.getParameterTypes()[0].isAssignableFrom(prop.getUserObject().getClass())) {
        constructor = (Constructor<? extends PropertyDelegate>) c;
      }
    }
    if (constructor == null) {
      throw new NoSuchMethodException("Unable to find a constructor for " + delegateClass.getClass() +
                                      " with " + prop.getUserObject().getClass() + " as only parameter.");
    }
    try {
      return constructor.newInstance(prop.getUserObject());
    } catch (InstantiationException |
             IllegalAccessException |
             IllegalArgumentException |
             InvocationTargetException ex) {
      throw new NoSuchMethodException(ex.getMessage());
    }
  }

  /**
   * Tries to get an adequate delegate for <tt>prop.getUserObject()</tt> by
   * checking its hierarchy of superclasses. If this does not help, a
   * <tt>TextDelegate</tt> will be returned. This method is the «non-lazy» part
   * of {@link #getDelegate(org.babelserver.property.Property) getDelegate(prop)}.
   * 
   * @since 4.1
   *
   * @return a delegate. If none is found, we return {@link TextDelegate}.
   */
  private Class<? extends PropertyDelegate> getDelegateAnyway(Property prop) {
    
    // check this common case first:
    if (prop.getUserObject() instanceof CharSequence) {
      return delegateMap.get(prop.getUserObject().getClass());
    }

    // Check superclasses:
    Set<Class<?>> registeredClasses = delegateMap.keySet();
    for (Class<?> c : registeredClasses) {
      if (c.isInstance(prop.getUserObject())) {
        return delegateMap.get(c);
      } 
    }

    // if everything else fails, return the default class:
    return delegateMap.get(CharSequence.class);
  }

}
