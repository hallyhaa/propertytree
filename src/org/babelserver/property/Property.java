/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Vector;
import javax.swing.CellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Property objects are tree nodes or list cells consisting of both a
 * description string and a property that may be manipulated in the GUI.
 * From a user perspective, using Property objects has three important parts:
 * the property itself (which will be represented by a Swing GUI delegate
 * like a JCheckBox, a JTextField or something similar), a description
 * (i.e. the text that is displayed next to the delegate) and finally a
 * PropertyController, i.e. an object that is to be notified about changes
 * to the property. The PropertyController may be {@code null}.
 * <P>
 * The Swing GUI components used are:
 * <UL>
 * <LI>JComboBoxes for Object[]</LI>
 * <LI>JCheckBoxes for booleans and Boolean objects</LI>
 * <LI>JColorChoosers for Color objects</LI>
 * <LI>Specialized JFormattedTextFields for Dimensions and Dates</LI>
 * <LI>JTextFields for Strings</LI>
 * </UL>
 * Other objects are {@code toString()}ed and put in JTextFields.
 * <P>
 * Previous versions of this class permitted Property objects to be constructed 
 * with no value to display besides the description string. Such use is 
 * deprecated from version 3.0. If you need tree nodes or list cells consisting 
 * of sole strings, either put the string in the <tt>value</tt> field and keep 
 * the <tt>description</tt> empty (both <tt>""</tt> and <tt>null</tt> are fine),
 * or use {@code DefaultMutableTreeNode} (in a tree) and whatever you please in
 * lists.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class Property extends DefaultMutableTreeNode {

  /**
   * Used to tell the Property object that something has changed in a delegate object.
   */
  public static final String PROPERTY_DELEGATE_CHANGE = "PropertyDelegateChange";

  /** Standard method name in PropertyController objects. */
  public static final String PROPERTY_CHANGE_CALLBACK_METHOD_NAME = "propertyChangeCallback";

  private String description;

  /** transient in super class, be we do need it! (30.12.2013: What for?) */
  protected Object	userObject;

    // TODO: java.util.Vector is obsolete. Update to something modern.
  /** 
   * You don't want to serialize a PropertyTree. If you do, you're probably just
   * looking for the tree model, so go there with your serialization instead. If,
   * however, you for some reason still want to serialize the PropertyTree itself,
   * remember that this vector will be re-initialized (and thereby be empty) upon 
   * de-serialization. No listeners follow through the serialization process,
   * so a deserialized PropertyTree is no good.
   */
  private transient Vector<Method> boundMethods = new Vector<>();
  /** 
   * You don't want to serialize a PropertyTree. If you do, you're probably just
   * looking for the tree model, so go there with your serialization instead. If,
   * however, you for some reason still want to serialize the PropertyTree itself,
   * remember that this vector will be re-initialized (and thereby be empty) upon 
   * de-serialization. No listeners follow through the serialization process,
   * so a deserialized PropertyTree is no good.
   */
  private transient Vector<Object> boundObjects = new Vector<>();

  /**
   * Creates a new Property object.
   * <P>
   * Don't use this constructor.
   *
   * (But then why is it here??)
   */
  private Property() {
    this("Wrong constructor, mate!", null, null);
  }

  /**
   * Creates a new Property object.
   *
   * @param description a description of the property.
   * A <tt>null</tt> value is accepted, in which case the Property
   * will be more or less equal to an ordinary TreeNode object.
   * @param value the property itself. This may not be <tt>null</tt>.
   */
  public Property(String description, Object value) {
    super(value);

    if (value == null) {
      throw new IllegalArgumentException("Value argument may not be null.");
    }

    if (description == null) {
      description = "";
    }
    this.description = description;

    setUserObject(value);

    if (value instanceof Object[]) {
      DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<>((Object[]) value);
      setUserObject(model);
    }
  }

  /**
   * Creates a new Property object and binds it to {@code callbackObject}.
   *
   * @param description a description of the property
   * @param value the property itself. This may be just about anything.
   * A <code>null</code> value is accepted, in which case the Property
   * will be more or less equal to an ordinary TreeNode object.
   * @param callbackObject the callback object bound to this property.
   * {@code callbackObject} will get a message every time there is a change
   * to <code>value</code>.
   * @see PropertyController
   */
  public Property(String description, Object value, PropertyController callbackObject) {
    this(description, value);
    assignPropertyController(callbackObject);
  }

  /**
   * Creates a new Property object and binds it to
   * {@code callbackObject}.
   *
   * @param description a description of the property
   * @param value the property itself. This may be just about anything.
   * A <code>null</code> value is accepted, in which case the Property
   * will be more or less equal to an ordinary TreeNode object.
   * @param callbackObject the callback object bound to this property.
   * {@code callbackObject} will get a message every time there is a change
   * to <code>value</code>. This parameter may be <code>null</code>,
   * indicating that no object is surveiling this property.
   * @param callbackMethod 
   */
  public Property(String description, Object value, Object callbackObject, Method callbackMethod) {
    this(description, value);
    if (callbackObject == null) {
      throw new IllegalArgumentException("callbackObject cannot be null");
    }
    if (callbackMethod == null) {
      throw new IllegalArgumentException("callbackMethod cannot be null");
    }
    
    bindToMethod(callbackObject, callbackMethod);
  }
  
  /**
   * Binds a callback object to the <tt>Property</tt>. If the instance is
   * created without a callback object, this method permits to assign one
   * dynamically.
   *
   * @param callbackObject the callback object to assign. If this parameter is
   * <tt>null</tt> or if it is not a <tt>PropertyController</tt>, an
   * exception is thrown.
   * 
   * @since 4.0
   */
public void assignPropertyController(PropertyController callbackObject) {
    if (callbackObject == null) {
      throw new IllegalArgumentException("callbackObject cannot be null");
    }
    try {
      Method m = callbackObject.getClass().getMethod(PROPERTY_CHANGE_CALLBACK_METHOD_NAME, new Class[]{PropertyChangeEvent.class});
      bindToMethod(callbackObject, m);
    } catch (NoSuchMethodException e) {
      throw new IllegalArgumentException("Unable to bind to callback object." +
                                         " (Does the " + callbackObject.getClass().getName() + 
                                         " class have a " + PROPERTY_CHANGE_CALLBACK_METHOD_NAME + 
                                         " method?)", e);
    }
  }

  /**
   * Binds this {@code Property} to a method so that whenever the {@code Property}
   * changes, <i>{@code method}</i> will be called on {@code instance}. This is
   * useful if you don't want to use a {@code PropertyController} or if you'd like
   * to have customized method names to take care of property changes (e.g. methods
   * with classical get/set names). <B>Note that {@code method} needs to take one 
   * and only one argument, and that one argument has to be of the same type as the 
   * property in this {@code Property} object.</B> If {@code method} does not meet 
   * this requirement, nothing will happen when the property changes (except for an 
   * exception being thrown).
   * <P>
   * <B>Remark:</B> While a PropertyController will receive {@code PropertyChangeEvent}s
   * for changes to the Property object, objects bound via this method will only
   * receive the new value.
   *
   * @param instance an instance of the object that has the method to be called.
   * @param method the method to be called. Must be a one argument method, and the
   * argument must be of the same type as the value passed to the Property object
   * constructor upon creation. Programmers are responsible for this, the
   * {@code Property} object itself performs no checking.
   * @since 2.0
   */
  public void bindToMethod(final Object instance, final Method method) {
    
    boundObjects.add(instance);
    boundMethods.add(method);

  }
  // TODO: Should we have a release method too, to release bindings?

  /**
   * Returns the description part of the Property.
   * When the Property object is a «regular» tree node,
   * it will contain only a description and nothing more.
   *
   * @return the description of this property.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description part of this property's
   * graphical representation.
   *
   * @param description a string.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Binds the <tt>Property</tt> to a listening object. When a {@code PropertyChangeEvent}
   * is fired from the graphical representation of this Property, a method is called on
   * the callbackObject, and the new value is passed with a <tt>PropertyChangeEvent</tt>.
   * This method is called from within the <tt>getTreeCellEditorComponent</tt> and 
   * <tt>getListCellEditorComponent</tt> methods in the CellEditor objects.
   *
   * @param target the JComponent to bind to. This is received as a final parameter,
   * because it is used in an anonymous inner class.
   * @param editor the CellEditor that calls this method. We need to know
   * since we plan on calling the {@code stopCellEditing()} method.
   */
  public void attachPropertyChangeListeners(final JComponent target, final CellEditor editor) {
    if (getUserObject() == null) {
      // Makes no sense to bind a variable. Go home!
      return;
      // Should we throw an exception?
    }
    
    if (boundObjects.size() != boundMethods.size()) {
      // Impossible!! But still, this is where we trust noone, not even ourselves!
      boundMethods = new Vector<Method>();
      boundObjects = new Vector<Object>();
      return;
      // Should we throw an exception?
    }
    
    if (boundObjects.isEmpty()) {
      // no bound object --> only accept modification, no need to alert anyone.
      target.addPropertyChangeListener(Property.PROPERTY_DELEGATE_CHANGE, new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent e) {
          editor.stopCellEditing(); // why do we need this?
        }
      });
    } else {
      for (int i=0; i<boundObjects.size(); i++) {

        final Object object = boundObjects.elementAt(i);
        final Method method = boundMethods.elementAt(i);

        target.addPropertyChangeListener(Property.PROPERTY_DELEGATE_CHANGE, new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent e) {
            try {
              if (object instanceof PropertyController && method.getName().equals(Property.PROPERTY_CHANGE_CALLBACK_METHOD_NAME)) {
                // PropertyControllers normally receive a PropertyChangeEvent
                // make new PropertyChangeEvent so that the Property object itself becomes the source
                e = new PropertyChangeEvent(Property.this, e.getPropertyName(), e.getOldValue(), e.getNewValue());
                method.invoke(object, e);
              } else {
                method.invoke(object, e.getNewValue());
              }
              editor.stopCellEditing();
            } catch (    IllegalAccessException | IllegalArgumentException | InvocationTargetException iaex) {
              // react to these exceptions somehow?
              //itex.printStackTrace();
            }
          }
        });
      }
    }
  }

  /**
   * DefaultMutableTreeNode overrides toString(). We can't have
   * that implementation, because DefaultMutableTreeNode sometimes
   * returns a null value. Note that <code>toString()</code> does
   * not return the same string as <code>paramString</code>, not
   * even very close.
   *
   * @return a string consisiting of the word «Property», the description
   * string and a string representation of the property value itself,
   * if any.
   */
  @Override
  public String toString() {
    String toReturn = "Property [" + description;
    if (getUserObject() != null) {
      toReturn += ": " + getUserObject().toString();
    }
    return toReturn + "]";
  }

  /**
   * Returns an ordinary parameterized representation of the
   * elements in this object. This is more verbose than the
   * toString() method.
   *
   * @return a parameterized representation of the elements in
   * this object
   */
  public String paramString() {
    return 
           new StringBuilder("[Property: description=")
            .append(description)
            .append(", userObject is ")
            .append((getUserObject() == null) ? "null" : "of class " + getUserObject().getClass().getName())
            .append(" (value is ")
            .append((getUserObject() == null) ? "null, of course" : getUserObject().toString())
            .append("]")
            
            .toString();
  }

  /**
   * This method is shorthand for
   * {@code ((DefaultComboBoxModel)getUserObject()).setSelectedItem(o)}.
   *
   * @param o the new selected value.
   */
  public void setSelectedValue(Object o) {
    if (getUserObject() instanceof DefaultComboBoxModel) {
      ((DefaultComboBoxModel) getUserObject()).setSelectedItem(o);
    }
  }

  /**
   * Returns selected object, if current property is an instance of
   * DefaultComboBoxModel and one of the values is set as selected.
   * This method is shorthand for {@code ((DefaultComboBoxModel)getUserObject()).getSelectedItem()}.
   *
   * @return the currently selected object, or {@code null} if there
   * is none or if the current property is not an instance of DefaultComboBoxModel.
   */
  public Object getSelectedValue() {
    if (getUserObject() instanceof DefaultComboBoxModel) {
      return ((DefaultComboBoxModel) getUserObject()).getSelectedItem();
    }
    return null;
  }

  private void writeObject(ObjectOutputStream out) throws IOException {
    // hent tremodellen og skriv den eksplisitt her?
     out.defaultWriteObject();
   }

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    // read and reconstruct the tree model...
     in.defaultReadObject();
     // reinitialize transient objects:
     boundMethods = new Vector<>();
     boundObjects = new Vector<>();
   }
  
}
