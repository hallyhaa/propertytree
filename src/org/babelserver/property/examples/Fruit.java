/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.babelserver.property.examples;

/**
 * Only for demonstration purposes.
 * The other six classes extend this one.
 */
public class Fruit {
}

class Apple extends Fruit {
}

class Apricot extends Fruit {
}

class Banana extends Fruit {
}

class Kiwi extends Fruit {
}

class Lime extends Fruit {
}

class Watermelon extends Fruit {
}

