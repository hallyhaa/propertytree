package org.babelserver.property.examples;

/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS �AS IS� AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellEditor;
import org.babelserver.property.Property;
import org.babelserver.property.PropertyController;

/**
 *
 * @author  Hallvard Ystad
 */
public class TestPanel extends JApplet implements PropertyController {

  /**
   * Creates the TestPanel.
   */
  public TestPanel() {
    // Custom LAF? Here you are:
    //try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); }
    //   catch (Exception e) { /* If this dosn't work, never mind. */ }
    //try { UIManager.setLookAndFeel(new SubstanceLookAndFeel()); }
    //   catch (Exception e) { /* If this dosn't work, never mind. */ }
    //try { UIManager.setLookAndFeel(new NimbusLookAndFeel()); }
    //   catch (Exception e) { /* If this dosn't work, never mind. */ }
    //try { UIManager.setLookAndFeel(new MultiLookAndFeel()); }
    //   catch (Exception e) { /* If this dosn't work, never mind. */ }
    
    // NetBeans sets things up for us:
    initComponents();

    // Test a particular PropertyDelegate? Here's how:
    //try { 
    //  propertyTree1.registerDelegate(Fruit.class, FruitDelegateWithIcons.class); 
    //  propertyTree1.registerDelegate(File.class, FilenameDelegate.class); 
    //  propertyTree1.registerDelegate(String.class, HiddenTextDelegate.class); 
    //}  catch (NoSuchMethodException ex) { /* print an alert, maybe. */ }

    // finally a few custom arrangements:
    ownSetup();
  }


  // <editor-fold defaultstate="collapsed" desc=" ownSetup() : startup stuff ">
  private void ownSetup() {
    // Make sure we use a PropertyTreeModel where
    // this class is set as PropertyController on the nodes:
    propertyTree1.setModel(getPropertyTreeModel());

    // And ditto for the PropertyList:
    propertyList1.setModel(getPropertyListModel());

    // The ordinary JTree needs a CellEditorListener
    // to register (text!) changes:
    jTree1.getCellEditor().addCellEditorListener(new CellEditorListener() {

      @Override
      public void editingStopped(ChangeEvent e) {
        TreeCellEditor tce = (TreeCellEditor) e.getSource();
        String className = tce.getCellEditorValue().getClass().getName();
        className = className.substring(className.lastIndexOf(".") + 1);
        String changeString = TestPanel.this.jTree1.getLastSelectedPathComponent().toString() + " turned into " + tce.getCellEditorValue() + " (type: " + className + ")\n";
        addPanelText(changeString, Color.BLUE);
      }

      @Override
      public void editingCanceled(ChangeEvent e) {
        // no worries
      }
    });

    // The ordinary JList will have a listener
    // to respond to db-clicks
    jList1.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          addPanelText("JLists aren't editable\n", Color.BLUE);
        }
      }
    });
  } // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc=" getPropertyTreeModel() ">
  /**
   * Returns a DefaultTreeModel populated with example objects.
   * @return a DefaultTreeModel populated with example objects
   */
  protected DefaultTreeModel getPropertyTreeModel() {
    //DefaultMutableTreeNode root = new DefaultMutableTreeNode("PropertyTree");
    Property root = new Property("", "PropertyTree", this);
    Property parent;

    /* Remark: the Property objects underneath are constructed
     * with three parameters: 1) a description, 2) a property and
     * 3) «this». We supply ourselves (this class) as a
     * PropertyController so that we will be notified about
     * changes to the different Property nodes.
     *
     * Try replacing <tt>this</tt> with <tt>null</tt> in a node and see the
     * difference when you run the file!
     */

    parent = new Property("", "Names (Strings)", this);
    parent.add(new Property("First name", "John", this));
    parent.add(new Property("Family name", "Doe", this));
    root.add(parent);

    parent = new Property("", "Opinions (booleans)", this);
    parent.add(new Property("I like this tree", true, this));
    parent.add(new Property("I dislike this tree", false, this));
    root.add(parent);

    parent = new Property("", "Colours", this);
    parent.add(new Property("Red", Color.RED, this));
    parent.add(new Property("Blue", Color.BLUE, this));
    parent.add(new Property("Green", Color.GREEN, this));
    root.add(parent);

    parent = new Property("", "Screen resolutions (Dimensions)", this);
    parent.add(new Property("Small", new Dimension(1280, 1024), this));
    parent.add(new Property("Medium", new Dimension(1920, 1080), this));
    parent.add(new Property("Big", new Dimension(3840, 2160), this));
    root.add(parent);

    parent = new Property("", "Fortune (ComboBoxes)", this);
    String[] fortune;
    fortune = new String[]{"Put no trust in cryptic comments", "She's genuinely bogus", "Optimization hinders evolution"};
    parent.add(new Property("Short ones", fortune, this));
    fortune = new String[]{"The only problem with being a man of leisure is that you can never stop and take a rest", "New members are urgently needed in the Society for Prevention of Cruelty to Yourself. Apply within.", "Always borrow money from a pessimist; he doesn't expect to be paid back"};
    Property child = new Property("Longer ones", fortune, this);
    child.setSelectedValue(fortune[2]);
    parent.add(child);
    root.add(parent);

    parent = new Property("", "Ordinary strings", this);
    parent.add(new Property("Ordinary property", "You may change this text", this));
    parent.add(new Property("", "«One string editable» (like above, but with no description part)", this));
    //parent.add(new Property("«One string editable» with no callback", "", this)); // like below. Easier to use DefaultMutableTreeNode, I believe
    parent.add(new DefaultMutableTreeNode("DefaultMutableTreeNode - changeable, but with no callback"));
    root.add(parent);

    parent = new Property("", "Constitutions (Dates)", this);
    Calendar cal = Calendar.getInstance(); // uses default locale
    cal.set(1958, 9, 4);
    parent.add(new Property("France", cal.getTime(), this));
    cal.set(1814, 4, 17);
    parent.add(new Property("Norway", cal.getTime(), this));
    cal.set(1787, 6, 17);
    Property p = new Property("USA", cal.getTime(), this);
    parent.add(p);
    root.add(parent);

    parent = new Property("", "Files", this);
    File file = new File("/probably/doesn't/exist");
    parent.add(new Property("Inexistant", file, this));
    file = new File("/");
    parent.add(new Property("File System Root", file, this));
    root.add(parent);

    return new DefaultTreeModel(root);
  } // </editor-fold>

  // <editor-fold defaultstate="collapsed" desc=" getPropertyListModel() ">
  private DefaultListModel<DefaultMutableTreeNode> getPropertyListModel() {
    DefaultListModel<DefaultMutableTreeNode> model = new DefaultListModel<>();

    DefaultMutableTreeNode prop = new DefaultMutableTreeNode("PropertyList");
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Names (Strings)");
    model.addElement(prop);
    prop = new Property("First name", "John", this);
    model.addElement(prop);
    prop = new Property("Family name", "Doe", this);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Opinions (booleans)");
    model.addElement(prop);
    prop = new Property("I like this list", true, this);
    model.addElement(prop);
    prop = new Property("I dislike this list", false, this);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Colours");
    model.addElement(prop);
    prop = new Property("Red", Color.RED, this);
    model.addElement(prop);
    prop = new Property("Blue", Color.BLUE, this);
    model.addElement(prop);
    prop = new Property("Green", Color.GREEN, this);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Screen resolutions (Dimensions)");
    model.addElement(prop);
    prop = new Property("Small", new Dimension(800, 600), this);
    model.addElement(prop);
    prop = new Property("Medium", new Dimension(1152, 864), this);
    model.addElement(prop);
    prop = new Property("Big", new Dimension(1280, 1024), this);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Fortune (ComboBoxes)");
    model.addElement(prop);
    String[] fortune;
    fortune = new String[]{"Put no trust in cryptic comments", "She's genuinely bogus", "Optimization hinders evolution"};
    prop = new Property("Short ones", fortune, this);
    model.addElement(prop);
    fortune = new String[]{"The only problem with being a man of leisure is that you can never stop and take a rest", "New members are urgently needed in the Society for Prevention of Cruelty to Yourself. Apply within.", "Always borrow money from a pessimist; he doesn't expect to be paid back"};
    prop = new Property("Longer ones", fortune, this);
    model.addElement(prop);

    // The tree has this:
    //parent = new Property("", "Ordinary strings");
    //parent.add(new Property("Ordinary property", "You may change this text"));
    //parent.add(new Property("", "«One string editable» (like above, but with no description part)"));
    ////parent.add(new Property("«One string editable» with no callback", "")); // like below. Easier to use DefaultMutableTreeNode, I believe
    //parent.add(new DefaultMutableTreeNode("DefaultMutableTreeNode - changeable, but with no callback"));
    //root.add(parent);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Constitutions (Dates)");
    model.addElement(prop);
    Calendar cal = Calendar.getInstance(); // uses default locale
    cal.set(1958, 9, 4);
    prop = new Property("France", cal.getTime(), this);
    model.addElement(prop);
    cal.set(1814, 4, 17);
    prop = new Property("Norway", cal.getTime(), this);
    model.addElement(prop);
    cal.set(1787, 6, 17);
    prop = new Property("USA", cal.getTime(), this);
    model.addElement(prop);

    prop = new DefaultMutableTreeNode(" ");
    model.addElement(prop);
    prop = new DefaultMutableTreeNode("Files");
    model.addElement(prop);
    File file = new File("/probably/doesn't/exist");
    prop = new Property("Inexistant", file, this);
    model.addElement(prop);
    file = new File("/");
    prop = new Property("File System Root", file, this);
    model.addElement(prop);

    return model;
  } // </editor-fold>

  /**
   * Here's a main method that permits this JApplet extension to be used
   * as an application.
   *
   * @param args nevermind
   */
  public static void main(String[] args) {
    java.awt.EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.add(new TestPanel());
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
      }
    });
  }

  /**
   * This is the method that we need to be a proper PropertyController.
   * Every time a Property object is changed in the PropertyTree,
   * this method is called.
   */
  @Override
  public void propertyChangeCallback(PropertyChangeEvent e) {
    String className = ((Property) e.getSource()).getUserObject().getClass().getName();
    className = className.substring(className.lastIndexOf(".") + 1);
    String changeString = e.getOldValue() + " turned into " + e.getNewValue() + " (type: " + className + ")\n";
    addPanelText(changeString, Color.RED);
  }

  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jList1 = new javax.swing.JList();
    propertyList1 = new org.babelserver.property.PropertyList();
    jSplitPane1 = new javax.swing.JSplitPane();
    jPanel1 = new javax.swing.JPanel();
    jLabel2 = new javax.swing.JLabel();
    jScrollPane2 = new javax.swing.JScrollPane();
    propertyTree1 = new org.babelserver.property.PropertyTree();
    jPanel2 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    jTree1 = new javax.swing.JTree();
    jScrollPane3 = new javax.swing.JScrollPane();
    jTextPane1 = new javax.swing.JTextPane();
    jToggleButton1 = new javax.swing.JToggleButton();
    jToggleButton2 = new javax.swing.JToggleButton();

    jList1.setModel(new javax.swing.AbstractListModel() {
      String[] strings = { "Ordinary", "JLists", "Aren't", "Editable" };
      public int getSize() { return strings.length; }
      public Object getElementAt(int i) { return strings[i]; }
    });

    jSplitPane1.setResizeWeight(0.5);

    jPanel1.setPreferredSize(new java.awt.Dimension(354, 518));
    jPanel1.setVerifyInputWhenFocusTarget(false);

    jLabel2.setText("PropertyTree");

    jScrollPane2.setViewportView(propertyTree1);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(67, 67, 67)
            .addComponent(jLabel2))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addGap(20, 20, 20)
        .addComponent(jLabel2)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
        .addContainerGap())
    );

    jSplitPane1.setLeftComponent(jPanel1);

    jLabel1.setText("Ordinary JTree");

    jTree1.setEditable(true);
    jScrollPane1.setViewportView(jTree1);

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(61, 61, 61)
        .addComponent(jLabel1))
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 415, Short.MAX_VALUE)
        .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(20, 20, 20)
        .addComponent(jLabel1)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
        .addContainerGap())
    );

    jSplitPane1.setRightComponent(jPanel2);

    jScrollPane3.setViewportView(jTextPane1);

    jToggleButton1.setSelected(true);
    jToggleButton1.setText("<HTML>Property<B>Tree</B>");
    jToggleButton1.setAlignmentX(0.5F);
    jToggleButton1.setPreferredSize(new java.awt.Dimension(120, 23));
    jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jToggleButton1ActionPerformed(evt);
      }
    });

    jToggleButton2.setText("<HTML>Property<B>List</B>");
    jToggleButton2.setAlignmentX(0.5F);
    jToggleButton2.setPreferredSize(new java.awt.Dimension(120, 23));
    jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jToggleButton2ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jScrollPane3)
          .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
          .addGroup(layout.createSequentialGroup()
            .addComponent(jToggleButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jToggleButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jSplitPane1)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jToggleButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap())
    );
  }// </editor-fold>//GEN-END:initComponents
  private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
    jToggleButton1.setSelected(!jToggleButton1.isSelected());
    toggleTreeAndList(jToggleButton1.isSelected());
}//GEN-LAST:event_jToggleButton2ActionPerformed

private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
  // 1 = tree, 2 = list
  jToggleButton2.setSelected(!jToggleButton2.isSelected());
  toggleTreeAndList(jToggleButton1.isSelected());
}//GEN-LAST:event_jToggleButton1ActionPerformed

private void toggleTreeAndList(boolean tree) {
  if (tree) {
    jScrollPane2.setViewportView(propertyTree1);
    jScrollPane1.setViewportView(jTree1);
    jLabel2.setText("PropertyTree");
    jLabel1.setText("Ordinary JTree");
  } else {
    jScrollPane2.setViewportView(propertyList1);
    jScrollPane1.setViewportView(jList1);
    jLabel2.setText("PropertyList");
    jLabel1.setText("Ordinary JList");
  }
  this.validate();
}

private void addPanelText(String s, Color c) {
  Document d = jTextPane1.getDocument();
  SimpleAttributeSet sas = new SimpleAttributeSet();
  StyleConstants.setForeground(sas, c);
  jTextPane1.setParagraphAttributes(sas, true);
  try {
    d.insertString(d.getLength(), s, sas);
  } catch (BadLocationException ex) {
    ex.printStackTrace();
  }
}


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JList jList1;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JSplitPane jSplitPane1;
  private javax.swing.JTextPane jTextPane1;
  private javax.swing.JToggleButton jToggleButton1;
  private javax.swing.JToggleButton jToggleButton2;
  private javax.swing.JTree jTree1;
  private org.babelserver.property.PropertyList propertyList1;
  private org.babelserver.property.PropertyTree propertyTree1;
  // End of variables declaration//GEN-END:variables
  
}
