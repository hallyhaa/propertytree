/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.babelserver.property.examples;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.babelserver.property.Property;
import org.babelserver.property.delegate.TextDelegate;

/**
 * This class is only for showing the icon handling. We
 * extend <tt>TextDelegate</tt> in order to have only the
 * two methods we need in here.
 */
public class FruitDelegateWithIcons extends TextDelegate {
  
  /**
   * Even though we only want to override the description
   * label method underneath, we always need a one parameter
   * constructor in the delegate classes.
   */
  public FruitDelegateWithIcons(Fruit fruit) {
    super(fruit.getClass().getSimpleName());
  }
  
  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    Object o = property.getUserObject();
    if (!(o instanceof Fruit)) {
      return toManipulate; // if one doesn't mix up the delegates, this should never happen.
    }
    
    // What kind of fruit do we represent?
    String fruitName = o.getClass().getSimpleName().toLowerCase();
    
    // any logic could be implemented based on class type or whatever else.
    
    // Get a suitable icon:
    Image i = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/org/babelserver/property/examples/" + fruitName + ".png"));
    toManipulate.setIcon(new ImageIcon(i));

    return toManipulate;
  }
}
