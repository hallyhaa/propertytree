/**
 * Classes in this package are for documentation purposes only. This
 * whole package can be safely deleted. None of the classes are
 * needed for a <tt>PropertyTree</tt> or a <tt>PropertyList</tt> to
 * work, so if you need a jar file smaller than the one provided by
 * default, remove this package and pack for yourself.
 * <p>
 * You may copy and use whatever you like from here. Make sure to
 * look at how things are done in these files if you wonder how to
 * achieve something, the classes are well documented in the code.
 * <p>
 * Very little code is needed to use a PropertyTree. In fact, the
 * construction of a PropertyTree doesn't require any more code than
 * the construction of an ordinary JTree. Let's say the first setting
 * you want to put in your tree is a boolean value, e.g. representing
 * whether or not a user wants to send bug reports for your application
 * automatically. You would need to create the boolean object
 * representing the setting, and then you would already be ready to
 * construct the  PropertyTree itself. So the code <i>could</i> be as
 * simple as in the
 * <a href="https://bitbucket.org/hallyhaa/propertytree/wiki/Home#markdown-header-installation-and-use">wiki</a>.
 * But to gain more control, this code is probably more useful:
 * <code><pre>
   // First make your PropertyController so you can track changes:
   PropertyControllerImpl controller = new PropertyControllerImpl();

   // Then make the tree model. Using Java's own DefaultTreeModel is
   // OK. Wether the string you pass in is parameter #1 or #2 doesn't
   // really matter for the representation of the tree. (Only upon
   // editing the cell there is a difference. Try it out and see for
   // yourself!)
   Property root = new Property("", "Application settings");

   // now make a settings category:
   Property categoryBugs = new Property("", "Bug handling");
   // ... and add it to the root element:
   root.add(categoryBugs);

   // And then we will finally make the property object:
   Property autoSendBugReport = 
                new Property("Automatically report bugs", true, controller);
   // ... and add it to the right category:
   categoryBugs.add(autoSendBugs);

   // turn the Property object tree structure into a TreeModel:
   DefaultTreeModel model =  new DefaultTreeModel(root);
  
   // construct your tree and set the model:
   PropertyTree tree = new PropertyTree();
   tree.setModel(model);
   </pre></code>
 * Note that the PropertyController is added to the Property
 * constructor so that we receive callback events whenever the
 * user changes the Property.
 * <p>
 * When PropertyTrees and PropertyLists are displayed on screen, they
 * follow the same pattern as JTrees and JLists. All branches (call them
 * nodes/leaves/cells as you like), are painted using the same JComponent
 * over and over again. The big difference between the original JTrees and
 * the PropertyTree project resources, is that whereas a JTree will represent
 * anything non-String as a String through the toString() method, we have
 * delegate objects to generate the right sort of representation for the
 * Object. And since CellRenderers extend JLabels and JPanels, we are free
 * to use other swing components inside. This is just as important as the
 * PropertyController interface, since this is what customizes the appearance
 * of Properties in trees and lists. This appearance may be customized further.
 * Delegate objects may be overridden, supplementet or removed. Read on to 
 * see how.
 * <p>
 * In this java package (<tt>org.babelserver.property.examples</tt>), there
 * are some examples of PropertyDelegate classes changing the look of tree
 * or list nodes. First there is the 
 * {@link org.babelserver.property.examples.HiddenTextDelegate HiddenTextDelegate}
 * class. In its code, you'll find a <tt>static final JLabel</tt> field, and
 * you'll see that all calls to the <tt>getRenderer(...)</tt> method returns
 * this field. It <i>is</i> that simple. We've now hidden the contents of the
 * text that this Property object represents. Of course, since this delegate
 * simply extends the standard <tt>TextDelegate</tt> class, creating an editor
 * for the text is left to its parent, and therefore, the text is not hidden 
 * when being edited. This is very easy to change: just override the <tt>getEditor</tt>
 * method and return <tt>new JLabel("No more editing today");</tt>. Try!
 * <p>
 * The {@link org.babelserver.property.examples.FilenameDelegate FilenameDelegate}
 * is an example of how to have a <tt>File</tt> object represented in a PropertyTree
 * without the standard java JFileChooser popping up when users click in the tree.
 * Instead, the tree will accept typing directly. Replacing the default
 * <tt>FileDelegate</tt> object in the tree is a simple matter of a oneliner:
 * {@code mPropertyTree.registerDelegate(File.class, FilenameDelegate.class);} This
 * would leave File objects to a text editor so users may <i>type</i> file names
 * instead of picking them with the JFileChooser.
 * <p>
 * The {@link org.babelserver.property.examples.FruitDelegateWithIcons FruitDelegateWithIcons}
 * class shows how to pick a suitable icon for a JLabel in a JTree or a JList based
 * on the type of object to be represented. It's a fairly simple logic to check
 * class type and choose an icon based on the name of the class to represent. Only one
 * method has to be overridden to get there, and the result is pretty nice.
 * <p>
 * Please peek into the code, it will help form an impression about the PropertyTree
 * program flow. Check commented out sequences in the {@link TestPanel} and uncomment
 * them to see different examples in use. (For some of them, you'll have to implement
 * a little bit of the code yourself, but that too should be fairly simple.) More 
 * examples might show up sometime. And if you made a really cool Delegate object − 
 * hand it over, we might distribute it with the project package!
 * <p>
 * 
 * @since 4.2
 * 
 */
package org.babelserver.property.examples;
