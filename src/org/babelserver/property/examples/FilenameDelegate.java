/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.examples;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import org.babelserver.property.Property;
import org.babelserver.property.delegate.FileDelegate;
import org.babelserver.property.delegate.PropertyDelegate;
import org.babelserver.property.delegate.TextDelegate;

/**
 * If you do not wish to use the default {@link JFileChooser} to pick files, but
 * would like to let the user type in the file name directly, you could simply
 * replace the standard {@link FileDelegate} PropertyDelegate with the {@link TextDelegate},
 * thus: {@code Property.registerDelegate(File.class, TextDelegate.class);}.
 * <i>This one command is enough!</i> But then file names become simple strings,
 * and there is no checking that the strings even remotely resemble file paths.
 * <p>
 * This class is another way to do it. We copy what we like from the
 * <tt>TextDelegate</tt> class, (since we will do about exactly the same as a 
 TextDelegate PropertyDelegate,) and override the methods that we need to make a
 format check. Passing this class in as the PropertyDelegate for file (path)
 editing is still just one line: {@code Property.registerDelegate(File.class, FilenameDelegate.class);}.
 *
 * @author Hallvard Ystad
 */
public class FilenameDelegate extends JTextField implements PropertyDelegate {

  private File lastValue = null;

  /**
   * Instantiation should happen with file objects, not with Strings.
   *
   * @param file the <tt>File</tt> to display.
   */
  public FilenameDelegate(File file) {
    super(file.toString(), Math.max(10, file.toString().length()));
    addTheListener();
    setLastValue(file);
  }

  @Override
  public File getValue() {
    return new File(getText());
  }

  @Override
  public void setValue(Object o) {
    if (o instanceof File) {
      setText(o.toString());
    }
  }

  private void addTheListener() {
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (new File(getText()).exists()) {
          FilenameDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, getText());
          FilenameDelegate.this.setLastValue(new File(getText()));
        }
      }
    });
  }

  private void setLastValue(File file) {
    lastValue = file;
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    return new JLabel(getValue().getAbsolutePath());
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }

}
