/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.examples;

import javax.swing.JComponent;
import javax.swing.JLabel;
import org.babelserver.property.Property;
import org.babelserver.property.delegate.TextDelegate;

/**
 * This class illustrates how to hide an object in rendered mode.
 * We simply extends {@link TextDelegate} and override one single method.
 * In edit mode, the text is still visible, and all other functions are
 * intact ass well. This class comes into use instead of {@link TextDelegate}
 * simply by doing {@code Property.registerDelegate(CharSequence.class, HiddenTextDelegate.class);}.
 *
 * @author Hallvard Ystad
 */
public class HiddenTextDelegate extends TextDelegate {

  static final JLabel secret = new JLabel("Contents is secret (but editable!)");

  /**
   * There has to be a constructor with a CharSequence as sole parameter.
   * Although the <tt>TextDelegate</tt> class has plenty of those, we need
   * one here as well to be accepted as a proper <tt>PropertyDelegate</tt>.
   * @param value initial text
   */
  public HiddenTextDelegate(CharSequence value) {
    super(value);
  }

  @Override
  public JComponent getRenderer(JComponent tree, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    return secret;
  }

}
