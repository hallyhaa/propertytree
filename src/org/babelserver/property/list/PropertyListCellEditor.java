/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.list;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.babelserver.property.Property;
import org.babelserver.property.PropertyList;
import org.babelserver.property.delegate.DropDownDelegate;
import org.babelserver.property.delegate.PropertyDelegate;

/**
 * Makes sure list entries are displayed properly while being edited.
 * The getListCellEditorComponent method returns a JPanel for Property
 * cells, and a JTextField for other objects..
 *
 * @since 2.0
 * @author Hallvard Ystad
 */
public class PropertyListCellEditor extends DefaultCellEditor implements ListCellEditor {

  /** A <code>Property</code> object. */
  private Property myProperty = new Property("Dummy", "Dummy"); // just to make sure
  /** A panel that will contain the Property description and a JComponent delegate for the value. */
  private final JPanel panel = new JPanel(new BorderLayout(5, 0));

  /** 
   * A JLabel that will contain the description part of a Property object, or simply the
   * text of a TreeNode.
   */
  private final JLabel descriptionLabel = new JLabel();

  PropertyDelegate editingComponent = null;

  /**
   * Whether or not we're editing a {@code Property} object. "Ordinary"
   * list cells are treated by {@link javax.swing.DefaultCellEditor
   * DefaultCellEditor}
   */
  private boolean dealingWithAProperty = false;

  /**
   * Deprecated. Use the parameterless constructor instead.
   * 
   * @deprecated As of release 4.0.1, we no longer keep a reference
   *             to the PropertyList that uses this class. Please use
   *             {@link #PropertyListCellEditor()}.
   */
  @Deprecated
  public PropertyListCellEditor(PropertyList list) {
    this();
  }

  /**
   * Constructs a PropertyListCellEditor.
   * 
   * @since 4.0.1
   */
  public PropertyListCellEditor() {
    super(new JTextField());
  }

  /**
   * Returns the Component used for editing list cells. When <tt>value</tt>
   * is a Property object, this will be a JPanel. in other cases, we do as
   * JList's DefaultCellEditor, i.e. use a JTextField.
   *
   * @param list {@inheritDoc}
   * @param value {@inheritDoc}
   * @param row {@inheritDoc}
   * @param isSelected {@inheritDoc}
   * @param cellHasFocus {@inheritDoc}
   */
  @Override
  public Component getListCellEditorComponent(JList list, Object value, int row, boolean isSelected, boolean cellHasFocus) {

    dealingWithAProperty = value instanceof Property || value == null;

    Font font = null; //panel.getFont();
    //if (font == null) {
      if (list.getCellRenderer() != null && list.getCellRenderer() instanceof Component) {
        font = ((Component)list.getCellRenderer()).getFont();
      }
      if (font == null) {
        font = list.getFont();
      }
    //} // end if font == null

    // quick hack:
    if (!dealingWithAProperty) {
      delegate.setValue(value);
      editorComponent.setFont(font);
      return editorComponent;
    }

    panel.removeAll(); // safe, I believe
    panel.setFont(font); // do we really need this?
    descriptionLabel.setFont(font);

    panel.setComponentOrientation(list.getComponentOrientation());
    descriptionLabel.setComponentOrientation(list.getComponentOrientation());

    Color bg = null;
    Color fg = null;

    if (isSelected) {
      panel.setBackground(bg == null ? list.getSelectionBackground() : bg);
      panel.setForeground(fg == null ? list.getSelectionForeground() : fg);
    } else {
      panel.setBackground(list.getBackground());
      panel.setForeground(list.getForeground());
    }

    descriptionLabel.setForeground(panel.getForeground());
    descriptionLabel.setBackground(panel.getBackground());

//    if (dealingWithAProperty) { // don't need this check, since we return further up if !dealingWithAProperty
      myProperty = (Property) value;
      descriptionLabel.setText(myProperty.getDescription());
      editingComponent = null; // don't mess with leftovers from previous cell editors
      try {
        editingComponent = ((PropertyList)list).getDelegate(myProperty);
      } catch (Exception ex) {
        // you could e.g. ex.printStackTrace();
      }
      if (editingComponent != null) {
        myProperty.attachPropertyChangeListeners((JComponent) editingComponent, this);
        ((JComponent) editingComponent).setFont(font);
                                                                   // false, false = expanded, leaf (only relevant for trees)
        panel.add(editingComponent.getEditor(list, panel, isSelected, false, false, row, this), BorderLayout.CENTER);
        editingComponent.finishDescriptionLabel(list, descriptionLabel, isSelected, false, false, row, true, myProperty);

      } else {
        // no property here!
        Component component = new JTextField(myProperty.getDescription()); 
        descriptionLabel.setText(null);
        panel.add(component, BorderLayout.CENTER);
      }

    panel.add(descriptionLabel, BorderLayout.LINE_START);

    Border border = null;
    if (cellHasFocus) {
      if (isSelected) {
        border = UIManager.getBorder("List.focusSelectedCellHighlightBorder");
      }
      if (border == null) {
        border = UIManager.getBorder("List.focusCellHighlightBorder");
      }
    }
    panel.setBorder(border);
    return panel;
  }

  /**
   * Returns whatever value is currently in the editor.
   * This method is overridden in order to retrieve the
   * value from the right delegate component (we don't
   * use the same as the super component).
   *
   * @return whatever we get from {@link PropertyDelegate#getValue()
   * editingComponent.getValue()}.
   */
  @Override
  public Object getCellEditorValue() {
    if (!dealingWithAProperty) {
      return super.getCellEditorValue();
    }

    if (editingComponent instanceof DropDownDelegate) {
      myProperty.setSelectedValue(((DropDownDelegate) editingComponent).getSelectedItem());
      return myProperty.getUserObject();
    } else {
      return editingComponent.getValue();
    }
  }

}
