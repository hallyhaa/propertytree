/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.list;

import java.awt.Component;
import javax.swing.CellEditor;
import javax.swing.JList;

/**
 * Assures ListCellEditors have the extensions
 * needed to configure an editor in a JList.
 *
 * @since 2.0
 * @author Hallvard Ystad
 */
public interface ListCellEditor extends CellEditor {
  
  /**
   * Returns the component that should be added to the client's
   * Component hierarchy.  Once installed in the client's hierarchy,
   * this component will then be able to draw and receive user input.
   * The overridden method (in <code>DefaultTreeCellEditor</code>) returns
   * a JLabel, but this class will return a JPanel, in order to better
   * represent <code>Property</code> objects.
   *
   *
   * @param	list		the JList that is asking the editor to edit;
   *				this parameter may be null, but will mostly
   *                            be a PropertyList
   * @param	value		the value of the cell to be edited.
   * @param	row		the row index of the cell being edited
   * @param	isSelected	true if the cell is to be rendered with
   *				selection highlighting
   * @param	cellHasFocus	true if the cell has focus
   * @return	the editing component
   */
  Component getListCellEditorComponent(JList list, Object value, int row, boolean isSelected, boolean cellHasFocus);
  
}
