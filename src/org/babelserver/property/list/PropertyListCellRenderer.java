/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS �AS IS� AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.list;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.babelserver.property.Property;
import org.babelserver.property.PropertyList;
import org.babelserver.property.delegate.PropertyDelegate;

/**
 * Makes sure list entries are displayed.
 * <P>
 * The <code>DefaultListCellRenderer</code> extends <code>JLabel</code>
 * and "reuses itself" on every node in a list in order to perform better.
 * So, in ordinary JLists, there are no real "live cells", they are all
 * the same JLabel, only with new text at each repaint (each node). This 
 * class uses a JPanel instead, populating it with whatever components are
 * needed to represent different properties. ({@code Property} objects).
 *
 * @since 2.0
 * @author Hallvard Ystad
 */
public class PropertyListCellRenderer extends DefaultListCellRenderer {
  
  private JLabel descriptionLabel;
  private Property myProperty;
  /** A panel that will contain the Property description and a JComponent delegate for the value. */
  private JPanel panel = new JPanel(new BorderLayout(5,0));
  
  /**
   * Returns a new instance of PropertyListCellRenderer. Nothing
   * is changed here from the super class. Icons and text colour
   * are determined from the UIManager.
   */
  public PropertyListCellRenderer() {
    super();
    myProperty = new Property("Dummy", "Dummy");  // just in case
    descriptionLabel = new javax.swing.JLabel();
  }
  
  /**
   * Configures the renderer.
   *
   * @param list the JList to which we belong. This should always be a PropertyList
   * @param value the value of this node.
   * @param index The cells index.
   * @param isSelected True if the specified cell was selected.
   * @param cellHasFocus True if the specified cell has the focus.
   * @return A component whose paint() method will render the specified value.
   */
  @Override
  public Component getListCellRendererComponent(
    JList list, 
    Object value, 
    int index,
    boolean isSelected, 
    boolean cellHasFocus) {
    
    Font font = getFont();
    if(font == null) {
        font = getFont();
      if(font == null)
        font = list.getFont();
    }

    if (!(value instanceof Property)) {
      list.setFont(font);
      return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
    
    myProperty = (Property) value;
    descriptionLabel.setText(myProperty.getDescription());
    
    panel.setComponentOrientation(list.getComponentOrientation());
    descriptionLabel.setComponentOrientation(list.getComponentOrientation());
    
    Color bg = null;
    Color fg = null;
    
    if (isSelected) {
      panel.setBackground(bg == null ? list.getSelectionBackground() : bg);
      panel.setForeground(fg == null ? list.getSelectionForeground() : fg);
      descriptionLabel.setBackground(bg == null ? list.getSelectionBackground() : bg);
      descriptionLabel.setForeground(fg == null ? list.getSelectionForeground() : fg);
    } else {
      panel.setBackground(list.getBackground());
      panel.setForeground(list.getForeground());
      descriptionLabel.setBackground(list.getBackground());
      descriptionLabel.setForeground(list.getForeground());
    }
    
    setFont(font);
    panel.setFont(font);
    descriptionLabel.setFont(font);
    
    panel.removeAll(); // ok at the moment.
    if (!myProperty.getDescription().isEmpty()) {
      panel.add(descriptionLabel, BorderLayout.LINE_START);
    }
    
      PropertyDelegate editingComponent = null;  // don't mess with leftovers from previous cell renderers
      try {
        editingComponent = ((PropertyList)list).getDelegate(myProperty);
      } catch (Exception ex) {
        // you could e.g. ex.printStackTrace();
      }
      if (editingComponent != null) {
                                                                        // false = expanded, leaf (relevant for trees)
        JComponent oRenderer = editingComponent.getRenderer(list, myProperty, isSelected, false, false, index, cellHasFocus);
                                                                                               // index = row in trees
        if (oRenderer != null) {
          oRenderer.setFont(font);
          oRenderer.setForeground(fg);
          panel.add(oRenderer, BorderLayout.CENTER);
        }
        editingComponent.finishDescriptionLabel(list, descriptionLabel, isSelected, false, false, index, false, myProperty);
      }
    
    Border border = null;
    if (cellHasFocus) {
      if (isSelected) {
        border = UIManager.getBorder("List.focusSelectedCellHighlightBorder");
      }
      if (border == null) {
        border = UIManager.getBorder("List.focusCellHighlightBorder");
      }
    }
    panel.setBorder(border);
    
    return panel;
  }
  
}
