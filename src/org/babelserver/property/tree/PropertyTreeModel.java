/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.tree;

import javax.swing.tree.DefaultTreeModel;
import org.babelserver.property.Property;

/**
 * Forces use of Property objects as TreeNodes. This
 * class extends the DefaultTreeModel, and the only
 * thing it really does, is to make sure the only valid
 * object type passed to the constructor, is the
 * <code>Property</code> class.
 * <P>
 * PropertyTrees cannot have other models than this class
 * or subclasses of it.
 * 
 * @deprecated This class is no longer in use and may disappear
 * from future releases. PropertyTrees use whatever model they like. 
 * @author Hallvard Ystad
 */
public class PropertyTreeModel extends DefaultTreeModel {
  
  /**
   * Creates a new instance of PropertyTreeModel.
   * While the DefaultTreeModel accepts a TreeNode as
   * the root object, we only accept Property objects.
   *
   * @param root the Property object to figure as the 
   * tree root
   */
  public PropertyTreeModel(Property root) {
    super(root, false);
  }
  
}
