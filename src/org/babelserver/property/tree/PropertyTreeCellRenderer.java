/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.tree;

import java.awt.*;
import java.io.File;
import java.text.ParseException;
import java.util.*;
import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import org.babelserver.property.Property;
import org.babelserver.property.PropertyTree;
import org.babelserver.property.delegate.BooleanDelegate;
import org.babelserver.property.delegate.DropDownDelegate;
import org.babelserver.property.delegate.PropertyDelegate;
import org.babelserver.swingext.text.DimensionFormatter;

/**
 * <p>
 * Displays an entry in a tree. The <tt>PropertyTreeCellRenderer</tt> is
 * in most parts a copy of the {@link DefaultTreeCellRenderer}. In addition
 * to all the rendering and performance enhancing in the default implementation,
 * this class performs a little hocus pocus to allow different objects'
 * JPanel rendering.
 */
public class PropertyTreeCellRenderer extends JPanel implements TreeCellRenderer {
  /** Last tree the renderer was painted in. */
  private JTree tree;
  
  private JLabel descriptionLabel;
  private Property myProperty;

  /** Is the value currently selected. */
  protected boolean selected;
  /** True if has focus. */
  protected boolean hasFocus;
  /** True if draws focus border around icon as well. */
  private boolean drawsFocusBorderAroundIcon;
  /** If true, a dashed line is drawn as the focus indicator. */
  private boolean drawDashedFocusIndicator;
  
  // If drawDashedFocusIndicator is true, the following are used.
  /**
   * Background color of the tree.
   */
  private Color treeBGColor;
  /**
   * Color to draw the focus indicator in, determined from the background.
   * color.
   */
  private Color focusBGColor;
  
  // Icons
  /** Icon used to show non-leaf nodes that aren't expanded. */
  transient protected Icon closedIcon;
  
  /** Icon used to show leaf nodes. */
  transient protected Icon leafIcon;
  
  /** Icon used to show non-leaf nodes that are expanded. */
  transient protected Icon openIcon;
  
  // Colors
  /** Color to use for the foreground for selected nodes. */
  protected Color textSelectionColor;
  
  /** Color to use for the foreground for non-selected nodes. */
  protected Color textNonSelectionColor;
  
  /** Color to use for the background when a node is selected. */
  protected Color backgroundSelectionColor;
  
  /** Color to use for the background when the node isn't selected. */
  protected Color backgroundNonSelectionColor;
  
  /** Color to use for the focus indicator when the node has focus. */
  protected Color borderSelectionColor;
  
  private boolean isDropCell;
  
  /**
   * Returns a new instance of PropertyTreeCellRenderer.  Alignment is
   * set to left aligned. Icons and text color are determined from the
   * UIManager.
   */
  public PropertyTreeCellRenderer() {
    setLayout(new BorderLayout(5,0));
    setOpaque(false);
    
    myProperty = new Property("Dummy", "Dummy");  // just in case (but will we ever get nullpointers otherwise? really??
    descriptionLabel = new javax.swing.JLabel();
    descriptionLabel.setOpaque(false);
    descriptionLabel.setHorizontalAlignment(JLabel.LEFT);
    
    setLeafIcon(UIManager.getIcon("Tree.leafIcon"));
    setClosedIcon(UIManager.getIcon("Tree.closedIcon"));
    setOpenIcon(UIManager.getIcon("Tree.openIcon"));
    
    setTextSelectionColor(UIManager.getColor("Tree.selectionForeground"));
    setTextNonSelectionColor(UIManager.getColor("Tree.textForeground"));
    setBackgroundSelectionColor(UIManager.getColor("Tree.selectionBackground"));
    setBackgroundNonSelectionColor(UIManager.getColor("Tree.textBackground"));
    setBorderSelectionColor(UIManager.getColor("Tree.selectionBorderColor"));
    Object value = UIManager.get("Tree.drawsFocusBorderAroundIcon");
    drawsFocusBorderAroundIcon = (value != null && ((Boolean)value).booleanValue());
    value = UIManager.get("Tree.drawDashedFocusIndicator");
    drawDashedFocusIndicator = (value != null && ((Boolean)value).booleanValue());
  }
  
  private boolean overridePaint() {
    LookAndFeel f = UIManager.getLookAndFeel();
    return (f.getID().equalsIgnoreCase("Metal") ||
      f.getID().equalsIgnoreCase("Mac") ||
      f.getID().equalsIgnoreCase("Windows") ||
      f.getID().equalsIgnoreCase("Motif"));
  }
  
  /**
   * Returns the default icon, for the current laf, that is used to
   * represent non-leaf nodes that are expanded.
   */
  public Icon getDefaultOpenIcon() {
    return UIManager.getIcon("Tree.openIcon");
  }
  
  /**
   * Returns the default icon, for the current laf, that is used to
   * represent non-leaf nodes that are not expanded.
   */
  public Icon getDefaultClosedIcon() {
    return UIManager.getIcon("Tree.closedIcon");
  }
  
  /**
   * Returns the default icon, for the current laf, that is used to
   * represent leaf nodes.
   */
  public Icon getDefaultLeafIcon() {
    return UIManager.getIcon("Tree.leafIcon");
  }
  
  /**
   * Sets the icon used to represent non-leaf nodes that are expanded.
   */
  public void setOpenIcon(Icon newIcon) {
    openIcon = newIcon;
  }
  
  /**
   * Returns the icon used to represent non-leaf nodes that are expanded.
   */
  public Icon getOpenIcon() {
    return openIcon;
  }
  
  /**
   * Sets the icon used to represent non-leaf nodes that are not expanded.
   */
  public void setClosedIcon(Icon newIcon) {
    closedIcon = newIcon;
  }
  
  /**
   * Returns the icon used to represent non-leaf nodes that are not
   * expanded.
   */
  public Icon getClosedIcon() {
    return closedIcon;
  }
  
  /**
   * Sets the icon used to represent leaf nodes.
   */
  public void setLeafIcon(Icon newIcon) {
    leafIcon = newIcon;
  }
  
  /**
   * Returns the icon used to represent leaf nodes.
   */
  public Icon getLeafIcon() {
    return leafIcon;
  }
  
  /**
   * Sets the color the text is drawn with when the node is selected.
   */
  public void setTextSelectionColor(Color newColor) {
    textSelectionColor = newColor;
  }
  
  /**
   * Returns the color the text is drawn with when the node is selected.
   */
  public Color getTextSelectionColor() {
    return textSelectionColor;
  }
  
  /**
   * Sets the color the text is drawn with when the node isn't selected.
   */
  public void setTextNonSelectionColor(Color newColor) {
    textNonSelectionColor = newColor;
  }
  
  /**
   * Returns the color the text is drawn with when the node isn't selected.
   */
  public Color getTextNonSelectionColor() {
    return textNonSelectionColor;
  }
  
  /**
   * Sets the color to use for the background if node is selected.
   */
  public void setBackgroundSelectionColor(Color newColor) {
    backgroundSelectionColor = newColor;
  }
  
  
  /**
   * Returns the color to use for the background if node is selected.
   */
  public Color getBackgroundSelectionColor() {
    return backgroundSelectionColor;
  }
  
  /**
   * Sets the background color to be used for non selected nodes.
   */
  public void setBackgroundNonSelectionColor(Color newColor) {
    backgroundNonSelectionColor = newColor;
  }
  
  /**
   * Returns the background color to be used for non selected nodes.
   */
  public Color getBackgroundNonSelectionColor() {
    return backgroundNonSelectionColor;
  }
  
  /**
   * Sets the color to use for the border.
   */
  public void setBorderSelectionColor(Color newColor) {
    borderSelectionColor = newColor;
  }
  
  /**
   * Returns the color the border is drawn.
   */
  public Color getBorderSelectionColor() {
    return borderSelectionColor;
  }
  
  /**
   * Subclassed to map <code>FontUIResource</code>s to null. If
   * <code>font</code> is null, or a <code>FontUIResource</code>, this
   * has the effect of letting the font of the JTree show
   * through. On the other hand, if <code>font</code> is non-null, and not
   * a <code>FontUIResource</code>, the font becomes <code>font</code>.
   */
  @Override
  public void setFont(Font font) {
    if(font instanceof FontUIResource) {
      font = null;
    }
    super.setFont(font);
  }
  
  /**
   * Gets the font of this component.
   * @return this component's font; if a font has not been set
   * for this component, the font of its parent is returned
   */
  @Override
  public Font getFont() {
    Font font = super.getFont();
    
    if (font == null && tree != null) {
      // Strive to return a non-null value, otherwise the html support
      // will typically pick up the wrong font in certain situations.
      font = tree.getFont();
    }
    return font;
  }
  
  /**
   * Subclassed to map <code>ColorUIResource</code>s to null. If
   * <code>color</code> is null, or a <code>ColorUIResource</code>, this
   * has the effect of letting the background color of the JTree show
   * through. On the other hand, if <code>color</code> is non-null, and not
   * a <code>ColorUIResource</code>, the background becomes
   * <code>color</code>.
   */
  @Override
  public void setBackground(Color color) {
    if(color instanceof ColorUIResource) {
      color = null;
    }
    super.setBackground(color);
  }
  
  /**
   * Configures the renderer based on the passed in components.
   * The JPanel returned displays <i>either</i> a JLabel with
   * the value from an "ordinary" TreeNode, <i>or</i> both
   * the description and a JComponent managing a value, if the
   * passed in {@code value} is an instance of {@code Property}.
   * 
   * <P>
   * The nice thing about the {@code getTreeCellRendererComponent}
   * method, is that you don't have to call it. As long as this class
   * is set as a JTree's cell renderer 
   * ({@code tree.setCellRenderer(new PropertyTreeCellRenderer());}),
   * swing will deal with the rest.
   */
  // TODO: Work from the alignNodeValues method in PropertyTree
  //       may only be implemented within this method.
  //       possible to store a value in parent node so that
  //       we don't have to check all siblings over and over as
  //       each sibling is painted?
  @Override
  public Component getTreeCellRendererComponent(JTree tree, 
                                                Object value,
                                                boolean sel,
                                                boolean expanded,
                                                boolean leaf, 
                                                int row,
                                                boolean hasFocus) {
    
    this.tree = tree;
    this.hasFocus = hasFocus;
    
    Font font = getFont();
    descriptionLabel.setFont(font);
    
    boolean dealingWithAProperty = value instanceof Property;

    descriptionLabel.setText(dealingWithAProperty ?
      ((Property)value).getDescription() :
      tree.convertValueToText(value, sel, expanded, leaf, row, hasFocus));

    Color fg;
    isDropCell = false;
    
    JTree.DropLocation dropLocation = tree.getDropLocation();
    if (dropLocation != null
      && dropLocation.getChildIndex() == -1
      && tree.getRowForPath(dropLocation.getPath()) == row) {
      
      Color col = UIManager.getColor("Tree.dropCellForeground");
      if (col != null) {
        fg = col;
      } else {
        fg = getTextSelectionColor();
      }
      
      isDropCell = true;
    } else if (sel) {
      fg = getTextSelectionColor();
    } else {
      fg = getTextNonSelectionColor();
    }
    
    setForeground(fg);
    descriptionLabel.setForeground(fg);
    
    // There needs to be a way to specify disabled icons.
    if (!tree.isEnabled()) {
      setEnabled(false);
      if (leaf) {
        descriptionLabel.setDisabledIcon(getLeafIcon());
      } else if (expanded) {
        descriptionLabel.setDisabledIcon(getOpenIcon());
      } else {
        descriptionLabel.setDisabledIcon(getClosedIcon());
      }
    } else {
      setEnabled(true);
      if (leaf) {
        descriptionLabel.setIcon(getLeafIcon());
      } else if (expanded) {
        descriptionLabel.setIcon(getOpenIcon());
      } else {
        descriptionLabel.setIcon(getClosedIcon());
      }
    }
    setComponentOrientation(tree.getComponentOrientation());
    
    selected = sel;
    
    this.removeAll(); // ok at the moment.
    add(descriptionLabel, BorderLayout.LINE_START);
    
    if (dealingWithAProperty) {
      myProperty = (Property) value;
      PropertyDelegate editingComponent = null;  // don't mess with leftovers from previous cell renderers
      try {
        editingComponent = ((PropertyTree)tree).getDelegate(myProperty);
      } catch (Exception ex) {
        // you could e.g. ex.printStackTrace();
      }
      if (editingComponent != null) {
        JComponent oRenderer = editingComponent.getRenderer(tree, myProperty, sel, expanded, leaf, row, hasFocus);
        if (oRenderer != null) {
          oRenderer.setFont(font);
          oRenderer.setForeground(fg);
          add(oRenderer, BorderLayout.CENTER);
        }
        editingComponent.finishDescriptionLabel(tree, descriptionLabel, selected, expanded, leaf, row, false, myProperty);
      } else {
        // no property here!
      }
    } // end if dealing with a property
    return this;
  }
  
  /**
   * Paints the value.  The background is filled based on selected.
   */
  @Override
  public void paint(Graphics g) {
    if (overridePaint()) {
      Color bColor;
      
      if (isDropCell) {
        bColor = UIManager.getColor("Tree.dropCellBackground");
        if (bColor == null) {
          bColor = getBackgroundSelectionColor();
        }
      } else if (selected) {
        bColor = getBackgroundSelectionColor();
      } else {
        bColor = getBackgroundNonSelectionColor();
        if (bColor == null) {
          bColor = getBackground();
        }
      }
      
      int imageOffset = -1;
      if(bColor != null) {
        imageOffset = getLabelStart();
        g.setColor(bColor);
        if(getComponentOrientation().isLeftToRight()) {
          g.fillRect(imageOffset, 0, getWidth() - imageOffset, getHeight());
        } else {
          g.fillRect(0, 0, getWidth() - imageOffset, getHeight());
        }
      }
      
      if (hasFocus) {
        if (drawsFocusBorderAroundIcon) {
          imageOffset = 0;
        } else if (imageOffset == -1) {
          imageOffset = getLabelStart();
        }
        if(getComponentOrientation().isLeftToRight()) {
          paintFocus(g, imageOffset, 0, getWidth() - imageOffset, getHeight(), bColor);
        } else {
          paintFocus(g, 0, 0, getWidth() - imageOffset, getHeight(), bColor);
        }
      }
    }
    super.paint(g);
  }
  
  private void paintFocus(Graphics g, int x, int y, int w, int h, Color notColor) {
    Color       bsColor = getBorderSelectionColor();
    
    if (bsColor != null && (selected || !drawDashedFocusIndicator)) {
      g.setColor(bsColor);
      g.drawRect(x, y, w - 1, h - 1);
    }
    if (drawDashedFocusIndicator && notColor != null) {
      if (treeBGColor != notColor) {
        treeBGColor = notColor;
        focusBGColor = new Color(~notColor.getRGB());
      }
      g.setColor(focusBGColor);
      BasicGraphicsUtils.drawDashedRect(g, x, y, w, h);
    }
  }
  
  private int getLabelStart() {
    Icon currentI = descriptionLabel.getIcon();
    if(currentI != null && descriptionLabel.getText() != null) {
      return currentI.getIconWidth() + Math.max(0,descriptionLabel.getIconTextGap() - 1);
    }
    return 0;
  }
  
  /**
   * Overrides <code>JComponent.getPreferredSize</code> to
   * return slightly wider preferred size value.
   */
  @Override
  public Dimension getPreferredSize() {
    Dimension        retDimension = super.getPreferredSize();
    
    if(retDimension != null) {
      retDimension = new Dimension(retDimension.width + 3,
        retDimension.height);
    }
    return retDimension;
  }
  
  /**
   * This method is overridden for performance reasons
   * in the DefaultTreeCellEditor. Since we're a JPanel,
   * and not a JLabel, we must not override it. This
   * method simply forwards to {@code super.validate();}
   */
  @Override
  public void validate() { super.validate(); }
  
  /**
   * This method is overridden for performance reasons
   * in the DefaultTreeCellEditor. Since we're a JPanel,
   * and not a JLabel, we must not override it. This
   * method simply forwards to {@code super.invalidate();}
   */
  @Override
  public void invalidate() { super.invalidate(); }
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void revalidate() {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void repaint(long tm, int x, int y, int width, int height) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void repaint(Rectangle r) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   *
   * @since 1.5
   */
  @Override
  public void repaint() {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
    // Strings get interned...
    if ("text".equals(propertyName)
      || (("font".equals(propertyName) || "foreground".equals(propertyName))
      && oldValue != newValue
      && getClientProperty(javax.swing.plaf.basic.BasicHTML.propertyKey) != null)) {
      
      super.firePropertyChange(propertyName, oldValue, newValue);
    }
  }
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, byte oldValue, byte newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, char oldValue, char newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, short oldValue, short newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, int oldValue, int newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, long oldValue, long newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, float oldValue, float newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, double oldValue, double newValue) {}
  
  /**
   * Overridden for performance reasons.
   * See the <a href="#override">Implementation Note</a>
   * for more information.
   */
  @Override
  public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {}
  
  // ---------- methods needed to ressemble DefaultTreeCellRenderer (which extends JLabel)
  /**
   * Returns the amount of space between the text and the icon
   * displayed in the description label of this tree node. Shorthand
   * for {@code descriptionLabel.getIconTextGap()}.
   *
   * @return an int equal to the number of pixels between the text
   *         and the icon in the description label.
   */
  public int getIconTextGap() {
    return descriptionLabel.getIconTextGap();
  }
  
}
