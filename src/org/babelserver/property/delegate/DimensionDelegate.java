/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.text.DefaultFormatterFactory;
import org.babelserver.property.*;
import org.babelserver.swingext.text.DimensionFormatter;

/**
 * A JFormattedTextField confirming to the PropertyDelegate interface
 * and accepting only {@code Dimension}s. The {@code Format} for
 * this text field is {@link org.babelserver.swingext.text.DimensionFormatter}.
 A PropertyChangeEvent is fired for a Property object to catch every time
 the DimensionDelegate changes its value.

 //TODO: why is it that in an ordinary JTextField, when I call requestFocus(),
 the cursor appears at the end of the text field, but in this class, it
 turns up at the beginning?
 * @author Hallvard Ystad
 */
public class DimensionDelegate extends JFormattedTextField implements PropertyDelegate {

  private Dimension lastValue = null;

  /**
   * Creates a new DimensionEditor. Since this method
   * has no <code>Dimension</code> parameter, it will
   * construct a <code>DimensionEditor</code> object with
   * <code>new Dimension()</code>, which is equal to
   * <code>new Dimension(0,0)</code>.
   */
  public DimensionDelegate() {
    this(new Dimension());
  }

  /**
   * Creates a new DimensionEditor showing <code>dim</code>.
   *
   * @param dim the <code>Dimension</code> object to show in
   * the editor.
   */
  public DimensionDelegate(Dimension dim) {
    DimensionFormatter formatter = new DimensionFormatter();
    DefaultFormatterFactory aff = new DefaultFormatterFactory(formatter);
    setFormatterFactory(aff);
    setLayout(new java.awt.BorderLayout());
    addTheListener();
    super.setValue(dim);
    setLastValue(dim);
  }

  private void addTheListener() {

    this.addActionListener(new ActionListener() {

      /**
       * Can't use PropertyChangeListener, since we do not wish to have
       * alerts on simple focusLost events.
       */
      @Override
      public void actionPerformed(ActionEvent e) {
        DimensionDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, getValue());
        setLastValue(getValue());
      }
    });
  }

  @Override
  public Object getValue() {
    return super.getValue();
  }

  @Override
  public void setValue(Object o) {
    if (o instanceof Dimension) {
      super.setValue(o);
    }
  }

  /**
   * In order to track both the old and the new value, we are forced
   * to keep track of our 'old' dimension here. As for PropertyChangeListener
   * possibilities, look at the comment further up.
   *
   * @param d the dimension to be kept as the 'old value' for the
   * next property change.
   */
  private void setLastValue(Object d) {
    if (d instanceof Dimension) {
      lastValue = (Dimension) d;
    }
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    try {
      return new JLabel(new DimensionFormatter().valueToString(getValue()));
    } catch (ParseException ex) {
      // Quietly change to String representation
      return new JLabel(getValue().toString());
    }
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }
}
