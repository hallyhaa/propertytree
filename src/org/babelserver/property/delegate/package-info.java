/**
 * Classes in this package are default delegates in PropertyTrees and PropertyLists.
 * They may be extended to add or remove functionality, or they may be completely
 * replaced. In either case, the syntax for telling a PropertyTree to change a
 * delegate is {@code Property.registerDelegate(File.class, NewDelegate.class)}.
 * File.class is whatever object you wouldlike to present in a PropertyTree or a
 * PropertyList, and NewDelegate.class, of course, is the new delegate. The method
 * is static, so use it from wherever you like.
 * <p>
 * It is also possible to replace a delegate with null, in order to have it removed.
 * Default behaviour for PropertyTrees and PropertyLists when there is no delegate
 * to be found for an object, is to represent it as a <tt>String</tt>. This probably
 * will not look too good if your classes do not override the <tt>toString()</tt>
 * method.
 * <p>
 * Several examples of custom delegate objects are found in the
 * {@link org.babelserver.property.examples} package.
 */

package org.babelserver.property.delegate;
