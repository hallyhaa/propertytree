/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.babelserver.property.Property;
import org.babelserver.property.tree.PropertyTreeCellEditor;

/**
 * A PropertyDelegate is an interface able to render a Property graphically.
 * PropertyDelegates must implement the get and set methods that let a
 * Property object send and retrieve an Object to the delegate. As this object
 * (value) changes, the PropertyDelegate should fire a PropertyChangeEvent 
 * ({@link org.babelserver.property.Property#PROPERTY_DELEGATE_CHANGE}).
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public interface PropertyDelegate {
  
  /**
   * This method will be called by <code>Property</code>
   * objects to retrieve the delegate's current value. The value
   * might be a boolean, a dimension, a colour, a date, a file,
   * some text or anything else.
   * 
   * @return the delegate's current value
   */
  public Object getValue();
  
  /**
   * Added bacuase we've got the getValue() method. 
   * We like symmetry.
   * 
   * @param o whatever object we're dealing with.
   */
  public void setValue(Object o);

  /**
   * Creates a suitable {@link JLabel} to render the description part of a Property object.
   * This method is automatically called from the cell editor of a ProertyTree or a
   * PropertyList. The «real» property has two different methods for being displayed, one
   * for simple rendering (<tt>getRenderer</tt>) and one for editing (<tt>getEditor</tt>),
   * but the description part has got only this one method. In return, we have the
   * <tt>isEditing</tt> boolean parameter so that implementing classes may adjust their
   * behaviour as needed.
   * <p>
   * PropertyTrees and PropertyLists will have cell renderers and cell editors that do 
   * their own jobs with this JLabel, but we have a final say here. This means 1) that
   * if you do not want to deal with this, the cell renderer / editor will already have
   * made an acceptable JLabel and you may return it directly without even touching it,
   * and 2) that if you make changes to this JLabel, they will not be overridden.
   * <p>
   * If you are implementing this interface, but don't know what you are supposed
   * to do with all these parameters, there is no need to worry. You can safely
   * ignore all of them as long as you return a <tt>JComponent</tt> to edit the
   * {@code userObject}.
   *
   * @param treeOrList the JTree or JList that we are part of
   * @param toManipulate the JLabel that we shall tamper with
   * @param	selected true if the cell is to be rendered with selection highlighting
   * @param	expanded true if the node is expanded (for PropertyTrees - PropertyLists send dummy booleans)
   * @param	leaf	 true if the node is a leaf node (for PropertyTrees - PropertyLists send dummy booleans)
   * @param	row	 the row index of the node being edited
   * @param isEditing <tt>true</tt> if this method is called from a cell editor, <tt>false</tt> if from a renderer.
   * @param property the Property object we represent
   * @return a suitable delegate
   */
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property);

  /**
   * Creates a suitable {@link JComponent} object for editing the {@code userObject}
   * in a JTree. This method is automatically called from the cell editor of a ProertyTree 
   * or a PropertyList. Since it's work is almost that of the CellEditor's, we get almost
   * the same parameters as
   * {@link PropertyTreeCellEditor#getTreeCellEditorComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int)}.
   * The only exceptions are parameter #2, the <tt>Object</tt>, which is replaced by the
   * <tt>JComponent</tt> that is our parent in the tree, and the addition of a last
   * parameter, the {@link CellEditor}. Mostly, <tt>parent</tt> will be a JPanel wishing
   * for a JLabel, and <tt>cellEditor</tt> will be a {@link PropertyTreeCellEditor}.
   * <p>
   * If you are implementing this interface, but don't know what you are supposed
   * to do with all these parameters, there is no need to worry. You can safely
   * ignore all of them as long as you return a <tt>JComponent</tt> to edit the
   * {@code userObject}.
   *
   * @param treeOrList the JTree or JList that we are part of
   * @param parent the component that our component shall be added to
   * @param	selected true if the cell is to be rendered with selection highlighting
   * @param	expanded true if the node is expanded (for PropertyTrees - PropertyLists send dummy booleans)
   * @param	leaf	 true if the node is a leaf node (for PropertyTrees - PropertyLists send dummy booleans)
   * @param	row	 the row index of the node being edited
   * @param cellEditor
   * @return a suitable delegate
   */
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor);

  
  /**
   * Configures the renderer based on the passed in components. This method
   * is automatically called from the cell renderer of a PropertyTree or a 
   * PropertyList. It should return both the description of the <tt>Property</tt>
   * we represent and a JComponent managing its value.
   * <p>
   * As in the <tt>getEditor</tt> method, implementers of this interface doesn't have to
   * worry about all the parameters passed in. But if your representation of the
   * {@code userObject} depends on the state of the <tt>PropertyTree</tt> or of the
   * cell it is in, they could be useful.
   * 
   * @since 4.1
   * 
   * @param treeOrList the JTree or JList that we are part of
   * @param property the Property object we represent
   * @param sel
   * @param expanded
   * @param leaf
   * @param row
   * @param hasFocus
   * @return a JPanel would be nice, for instance.
   */
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus);
}
