/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.CellEditor;
import javax.swing.JColorChooser;
import static javax.swing.JColorChooser.createDialog;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTree;
import org.babelserver.property.*;

/**
 * A JColorDelegate confirming to the {@link PropertyDelegate} interface. A PropertyChangeEvent is fired for a Property object to catch every
 * time a new colour is picked and confirmed.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class ColorDelegate extends JColorChooser implements PropertyDelegate {

  public Color lastValue = null;

  /**
   * Creates a new ColorChooser instance. The represented Color value will default to {@link Color#WHITE}
   *
   * @since 4.1
   */
  public ColorDelegate() {
    this(Color.WHITE);
  }

  /**
   * Creates a new ColorChooser instance.
   *
   * @param c the <tt>Color</tt> to set initially.
   */
  public ColorDelegate(Color c) {
    super(c);
    lastValue = c;
    addTheListener();
  }

  private void addTheListener() {
    // taken care of in showDialog()
  }

  @Override
  public Color getValue() {
    return getColor();
  }

  @Override
  public void setValue(Object o) {
    if (o instanceof Color) {
      setColor((Color) o);
    }
  }

  /**
   * Replaces JColorDelegate's <tt>showDialog()</tt> method in order to assign a
   * listener to the OK button. Remark that this method is not static.
   * @param parent the parent component for the dialog
   * @return 
   */
  public boolean showChooserDialog(Component parent) {

    final Color original = getColor();

    final ActionListener al = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
          firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, getColor());
          lastValue = getColor();
        } else {
          setColor(original);
        }
      }
    };

    JDialog dialog = createDialog(parent, "Pick a Colour", true /* = modal */, this, al /* = OK button handler */, al /* = cancel button handler */);
    dialog.setVisible(true);
    return original.equals(getColor());
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, final JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, final CellEditor cellEditor) {
    new Thread() {
      @Override
      public void run() {
        if (showChooserDialog(parent)) {
          cellEditor.cancelCellEditing();
        } else {
          cellEditor.stopCellEditing();
        }
      }
    }.start();

    // as in the cell renderer:
    JLabel label = new JLabel("        ");
    label.setBackground(getColor());
    label.setOpaque(true); // else, the BG colour won't be visible

    return label;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    JLabel toReturn = new JLabel("        ");
    toReturn.setBackground(getColor());
    toReturn.setOpaque(true); // else, the BG colour won't be visible
    return toReturn;
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }

}
