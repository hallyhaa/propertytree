/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import org.babelserver.property.Property;

/**
 * A JFormattedTextField confirming to the PropertyDelegate interface
 * and accepting only <code>Date</code>s. The <code>Format</code> for 
 * this text field is <code>DateFormatter</code>. A PropertyChangeEvent 
 is fired for a Property object to catch every time the DateDelegate 
 changes its value.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class DateDelegate extends JFormattedTextField implements PropertyDelegate {
  
  private Date lastValue = null;

  /** 
   * Creates new DateEditor. This constructor is shorthand for
   * <code>DateEditor(Calendar.getInstance().getTime())</code>.
   */
  public DateDelegate() {
    this(Calendar.getInstance().getTime());
  }
  
  /** 
   * Creates a new <code>DateEditor</code> showing <code>date</code>. 
   * 
   * @param date the initial <code>Date</code> object.
   */
  public DateDelegate(Date date) {
    super();
    DateFormatter formatter = new DateFormatter();
    DefaultFormatterFactory aff = new DefaultFormatterFactory(formatter);
    setFormatterFactory(aff);
    //setLayout(new java.awt.BorderLayout()); // do we need this??
    addTheListener();
    super.setValue(date);
    setLastValue(date);
  }
  
  private void addTheListener() {
    this.addActionListener(new ActionListener() {

      /**
       * Can't use PropertyChangeListener, since we do not wish to have
       * alerts on simple focusLost events.
       */
      @Override
      public void actionPerformed(ActionEvent e) {
        DateDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, getValue());
        setLastValue(getValue());
      }
    });
  }
  
  @Override
  public Object getValue() {
    return super.getValue();
  }
  
  @Override
  public void setValue(Object o) {
    if (o instanceof Date)
      super.setValue((Date)o);
  }
 
  /**
   * In order to track both the old and the new value, we are forced
   * to keep track of our 'old' date here. As for PropertyChangeListener
   * possibilities, look at the comment further up.
   *
   * @param d the date to be kept as the 'old value' for the
   * next property change.
   */
  private void setLastValue(Object d) {
    if (d instanceof Date) {
      lastValue = (Date) d;
    }
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    try {
      return new JLabel(new DateFormatter().valueToString(getValue()));
    } catch (ParseException ex) {
      return new JLabel(getValue().toString());
    }
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }

}
