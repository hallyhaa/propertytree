/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTree;
import org.babelserver.property.*;

/**
 * A JFileDelegate confirming to the PropertyDelegate interface. A PropertyChangeEvent is fired for a Property object to catch every time a
 * new file is chosen.
 *
 * @since 4.0
 * @author Hallvard Ystad
 */
public class FileDelegate extends JFileChooser implements PropertyDelegate {

  private File lastValue = null;

  /**
   * Creates a new FileEditor instance. The represented file will be <tt>/</tt>, i.e. the root folder of the file system.
   *
   */
  public FileDelegate() {
    this(new File("/"));
  }

  /**
   * Creates a new FileEditor instance. The name "editor" may be misleading: this class actually is a JFileChooser implementing the
   * PropertyDelegate interface.
   *
   * @param file the <tt>File</tt> to display.
   */
  public FileDelegate(File file) {
    super(file);
    lastValue = file;
    addTheListener();
  }

  private void addTheListener() {
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(FileDelegate.APPROVE_SELECTION)) {
          FileDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, FileDelegate.this.getSelectedFile());
          lastValue = FileDelegate.this.getSelectedFile();
        }
      }
    });
  }

  @Override
  public File getValue() {
    return super.getSelectedFile();
  }

  @Override
  public void setValue(Object o) {
    if (o instanceof File) {
      super.setSelectedFile((File) o);
    }
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, final JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, final CellEditor cellEditor) {
    new Thread() {
      @Override
      public void run() {
        int returnValue = showOpenDialog(parent);
        if (returnValue == FileDelegate.APPROVE_OPTION) {
          cellEditor.stopCellEditing();
        } else {
          cellEditor.cancelCellEditing();
        }
      }
    }.start();
    return new JLabel(lastValue.toString());

  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    //return new JLabel(getValue().getAbsolutePath());
    return new JLabel(((File)property.getUserObject()).getAbsolutePath());
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }

}
