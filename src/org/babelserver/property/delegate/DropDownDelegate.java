/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.CellEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;
import org.babelserver.property.*;

/**
 * A JComboBox confirming to the PropertyDelegate interface.
 * A PropertyChangeEvent is fired for a Property object to catch every time
 * a new item is selected.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class DropDownDelegate extends JComboBox implements PropertyDelegate {
  
  private Object lastValue = null;
  
  /**
   * Creates a <code>DropDownEditor</code> with a default data model.
   * The default data model is a <tt>new DefaultComboBoxModel()</tt>,
   * a dummy object list which is more or less the same as an empty
   * <tt>Vector</tt> object.
   * 
   * @see #DropDownEditor(java.lang.Object[]) if you want to pass in
   * an object array.
   */
  public DropDownDelegate() {
    this(new DefaultComboBoxModel());
  }
  
  /**
   * Creates a <code>DropDownEditor</code> that contains the elements
   * in the specified array.  By default the first item in the array
   * (and therefore the data model) becomes selected.
   *
   * @param items  an array of objects to insert into the DropDownEditor
   */
  public DropDownDelegate(Object items[]) {
    this(new DefaultComboBoxModel(items));
  }
  
  /**
   * Creates a <code>DropDownEditor</code> that contains the elements
   * in the specified model.  By default the first item in the data
   * model becomes selected.
   *
   * @param aModel  a ComboBoxModel to insert into the DropDownEditor
   */
  public DropDownDelegate(ComboBoxModel aModel) {
    super(aModel);
    setEditable(false);
    setOpaque(false); // why?
    addTheListener();
    setLastValue(getSelectedItem());
  }

  private void addTheListener() {
    addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          DropDownDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, e.getItem());
          setLastValue(getValue());
        }
      }
    });
  }
  
  @Override
  public ComboBoxModel getValue() {
    return this.getModel();
  }
  
  /**
   * Implementing PropertyDelegate, we must have this method.
   * If {@code o} is a ComboBoxModel, it will be set as the
 DropDownDelegate's new model. If not, {@code setSelectedItem(o)}
   * will be called.
   * <P>
   * This method will mostly be used to set selected item, so even
   * when a ComboBoxModel is passed in, it will mostly be to change
   * the selection.
   *
   * @param o preferably a ComboBoxModel.
   */
  @Override
  public void setValue(Object o) {
    if (o instanceof ComboBoxModel) {
      this.setModel((ComboBoxModel)o);
    } else {
      setSelectedItem(o);
    }
  }
  
  /**
   * In order to track the old value for use when firing propertyChange
   * events, we are forced to keep track of our 'old' values here.
   *
   * @param o the object to be kept as the 'old value' for the next
   * property change.
   */
  private void setLastValue(Object o) {
    lastValue = o;
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    System.err.println("Editor for " + treeOrList);
    System.err.println("\tvalue: " + getValue());
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    return new JLabel(getValue().getSelectedItem().toString());
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }
  
}
