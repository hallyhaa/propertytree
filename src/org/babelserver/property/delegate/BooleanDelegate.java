/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.CellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTree;
import org.babelserver.property.*;

/**
 * A JCheckBox confirming to the PropertyDelegate interface.
 * As a user clicks the BooleanDelegate, a PropertyChange is
 fired for a Property object to catch.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class BooleanDelegate extends JCheckBox implements PropertyDelegate {
  
  /** 
   * Creates a BooleanEditor. The boolean value that we represent,
   * will default to <tt>false</tt>.
   */
  public BooleanDelegate() {
    this(false);
  }
  
  /** 
   * Creates a BooleanEditor.
   * 
   * @since 4.1
   * @param b
   */
  public BooleanDelegate(Boolean b) {
    setSelected(b);
    setOpaque(false);
    addTheListener();
  }
  
  private void addTheListener() {
    addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        BooleanDelegate.this.firePropertyChange("selected", !BooleanDelegate.this.isSelected(), BooleanDelegate.this.isSelected());
        BooleanDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, !BooleanDelegate.this.isSelected(), BooleanDelegate.this.isSelected());
      }
    });
  }
  
  @Override
  public Object getValue() {
    return isSelected();
  }
  
  @Override
  public void setValue(Object o) {
    if (o instanceof Boolean)
      setSelected((Boolean)o);
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    return this;
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }

}
