/*
 * Copyright (c) 2007, Petter Egesund, Hallvard Ystad.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Petter Egesund, Hallvard Ystad nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.babelserver.property.delegate;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.CellEditor;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.UIManager;
import org.babelserver.property.*;

/**
 * A JTextField confirming to the PropertyDelegate interface.
 * This delegate takes care of <tt>String</tt> and more generally
 * {@link CharSequence} objects. In addition, any other object
 * that a PropertyTree or a PropertyList is unable to couple with
 * a suitable delegate, will be handled by this class. The <tt>toString</tt>
 * method will be used for rendering the object in an ordinary JLabel.
 * A PropertyChangeEvent is fired whenever the text field looses
 * focus or an ActionEvent is fired.
 *
 * @author Petter Egesund
 * @author Hallvard Ystad
 */
public class TextDelegate extends JTextField implements PropertyDelegate {

  private String lastValue = null;

  /**
   * Creates new TextDelegate initially loaded with an empty string.
   * The number of columns will be set to 10.
   */
  public TextDelegate() {
    this("", 10);
  }

  /**
   * Creates new TextDelegate with a number
   * of columns corresponding to <code>value.toString().length()</code>.
   *
   * @param value the value (preferably a <code>String</code> to be shown)
   */
  public TextDelegate(CharSequence value) {
    this(value, value.length());
  }

  /**
   * Creates new TextDelegate with a number
   * of columns corresponding to <code>value.toString().length()</code>.
   *
   * @param value the value (preferably a <code>String</code> to be shown)
   */
  public TextDelegate(String value) {
    this(value, value.length());
  }

  /**
   * Creates new TextDelegate with a number
   * of columns corresponding to <code>value.toString().length()</code>.
   *
   * @param value the value (preferably a <code>String</code> to be shown)
   */
  public TextDelegate(Object value) {
    this(value, value.toString().length());
  }

  /**
   * Creates new TextDelegate with the specified number
   * of columns. This may be wider than the string's 
   * actual length, since the column width is calculated
   * from the width of the letter 'm'.
   *
   * @param value   the value (preferably a <code>String</code> to be shown)
   * @param columns the number of columns
   */
  public TextDelegate(Object value, int columns) {
    super(value.toString(), columns);
    addTheListener();
    setLastValue(value.toString());
  }

  private void addTheListener() {

    addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        TextDelegate.this.firePropertyChange(Property.PROPERTY_DELEGATE_CHANGE, lastValue, getText());
        setLastValue(getText());
      }
    });
  }

  @Override
  public CharSequence getValue() {
    return getText();
  }

  @Override
  public void setValue(Object o) {
    setText(o.toString());
  }

  /**
   * In order to track
   * both the old and the new value, we are forced
   * to keep track of our 'old' text here.
   *
   * @param string the string to be kept as the 'old value' for the
   * next property change.
   */
  private void setLastValue(String string) {
    lastValue = string;
  }

  @Override
  public JComponent getEditor(JComponent treeOrList, JComponent parent, boolean selected, boolean expanded, boolean leaf, int row, CellEditor cellEditor) {
    return this;
  }

  @Override
  public JComponent getRenderer(JComponent treeOrList, Property property, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
    return new JLabel(getValue().toString());
  }

  @Override
  public JLabel finishDescriptionLabel(JComponent treeOrList, JLabel toManipulate, boolean selected, boolean expanded, boolean leaf, int row, boolean isEditing, Property property) {
    return toManipulate;
  }
}
