# PropertyTree

The default Swing JTree implementation cannot contain graphical components like JCheckBoxes. 
The default JList implementation hasn't even got an «editable» mode to enable users to change
the contents of a list. The **PropertyTree** aimes at solving these issues by using
different JPanels instead of one single JLabel to paint the nodes and cells in a tree. The
result is a nice **PropertyTree** (and a nice **PropertyList**) that take care of your
property hierarchy.

This is what it looks like: ![screenshot](https://bitbucket.org/hallyhaa/propertytree/raw/b841e2e92b3a06bdf00512e328a1110c9a2b1321/www/img/screenshot.png)

Please refer to the [wiki](https://bitbucket.org/hallyhaa/propertytree/wiki) for documentation.